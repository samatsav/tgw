<?php

/**
 * @file
 * Provide the Discourse module admin page.
 */


/**
 * Discourse settings page callback.
 */
function discourse_admin_form($form, &$form_state) {
  $form['discourse_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Discourse URL'),
    '#description' => t('The web address of the Discourse server.'),
    '#size' => 40,
    '#default_value' => variable_get('discourse_server'),
    '#maxlength' => 120,
    '#required' => TRUE,
  );
  /*
  $form['discourse_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Api Key'),
    '#description' => t('The api key for the Discourse server.'),
    '#size' => 40,
    '#default_value' => variable_get('discourse_api_key'),
    '#maxlength' => 120,
    '#required' => FALSE,
  );
  $form['discourse_api_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Discourse Admin Username'),
    '#description' => t('The admin username for the Discourse server.'),
    '#size' => 40,
    '#default_value' => variable_get('discourse_api_username'),
    '#maxlength' => 120,
    '#required' => FALSE,
  );*/
  $form['discourse_drupal_user_sync'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sync Drupal users to Discourse accounts.'),
    '#description' => t('Automatically create Discourse accounts for any new Drupal user accounts, and log users into Discourse upon Drupal login.'),
    '#default_value' => variable_get('discourse_drupal_user_sync'),
  );
  $form['discourse_drupal_user_sync_now'] = array(
    '#type' => 'checkbox',
    '#title' => t('Create Discourse accounts for all Drupal accounts, matched by email.'),
    '#description' => t('Automatically create Discourse accounts for any existing Drupal user accounts.'),
    '#default_value' => NULL,
  );
  $form['discourse_header_as_block'] = array(
    '#type' => 'checkbox',
    '#title' => t('Provide the Discourse header as a block.'),
    '#description' => t('Removes the Discourse header (login, search etc) from the main content area and provides it as a Drupal block instead. You will have to manually enable it in the Drupal block admin screen.'),
    '#default_value' => variable_get('discourse_header_as_block'),
  );
  // @todo add option for discourse_forum_root and update discourse_menu().
  $form['#validate'][] = 'discourse_admin_form_validate';
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );


  return $form;
}


/**
 * Save configuration settings for the Discourse module.
 */
function discourse_admin_form_submit($form, &$form_state) {
  variable_set('discourse_server', $form_state['values']['discourse_server']);
  //variable_set('discourse_api_key', $form_state['values']['discourse_api_key']);
  //variable_set('discourse_api_username', $form_state['values']['discourse_api_username']);
  variable_set('discourse_drupal_user_sync', $form_state['values']['discourse_drupal_user_sync']);
  variable_set('discourse_header_as_block', $form_state['values']['discourse_header_as_block']);
  if ($form_state['values']['discourse_drupal_user_sync_now']) {
    include_once('discourse_user.inc');
    discourse_drupal_user_sync_now();
  }
  drupal_set_message(t('The settings have been saved'));
}


/**
 * Validation for the admin form.
 */
function discourse_admin_form_validate($form, &$form_state) {
  $url = $form_state['values']['discourse_server'];
  if (!valid_url($url, FALSE)) {
    form_set_error('discourse_server', t('Sorry, that does not appear to be a valid url.'));
  }
  // Ensure we have the http/https prefix.
  if (substr($url, 0, 4) != 'http') {
    $form_state['values']['discourse_server'] = 'http://' . $url;
  }
  // Remove any trailing slash.
  if (substr($form_state['values']['discourse_server'], -1) == '/') {
    $form_state['values']['discourse_server'] = substr($form_state['values']['discourse_server'], 0, -1);
  }
}
