<?php

/**
 * @file Handle Twitter external auth.
 */

include('common.inc');

/**
 * Setup Twitter auth redirect.
 */
function discourse_auth_twitter() {
  include(drupal_get_path('module', 'discourse') . '/lib/twitteroauth/twitteroauth.php');
  $config = discourse_auth_twitter_get_config();
  if (isset($_REQUEST['oauth_token']) && isset($_REQUEST['oauth_verifier'])) {
    discourse_auth_twitter_callback($config);
  }
  else {
    discourse_auth_twitter_redirect($config);
  }
}


/**
 * Perform Twitter auth redirect.
 */
function discourse_auth_twitter_redirect($config) {
  if (!drupal_session_started()) {
    drupal_session_start();
  }
  global $base_secure_url, $base_insecure_url;
  $host = (!empty($_SERVER['HTTPS'])) ? $base_secure_url : $base_insecure_url;
  $callback_url = $host . base_path() . variable_get('discourse_forum_root') .'/auth/twitter/callback';
  $twitter = new TwitterOAuth($config['key'], $config['secret']);
  $temporary_credentials = $twitter->getRequestToken($callback_url);
  $_SESSION['discourse']['auth']['twitter']['temp_creds'] = $temporary_credentials;
  $location =  $redirect_url = $twitter->getAuthorizeURL($temporary_credentials);
  header('Location: ' . $location);
  exit;
}


/**
 * Handle Twitter auth callback.
 */
function discourse_auth_twitter_callback($config) {
  include(drupal_get_path('module', 'discourse') . '/includes/discourse_user.inc');
  $twitter = new TwitterOAuth(
    $config['key'],
    $config['secret'],
    $_SESSION['discourse']['auth']['twitter']['temp_creds']['oauth_token'],
    $_SESSION['discourse']['auth']['twitter']['temp_creds']['oauth_token_secret']
  );
  $token_credentials = $twitter->getAccessToken($_REQUEST['oauth_verifier']);
  $twitter = new TwitterOAuth(
    $config['key'],
    $config['secret'],
    $token_credentials['oauth_token'],
    $token_credentials['oauth_token_secret']
  );
  $account = $twitter->get('account/verify_credentials');
  if ($account) {
    $discourse_user = discourse_get_user_by_twitter_id($account->id);
    if ($discourse_user) {
      if ($discourse_user['active']) {
        // User authenticated by Twitter and activated. Log them in.
        discourse_login($discourse_user);
        $data = array('authenticated' => TRUE);
      }
      else {
        // User has already authenticated with Twitter, but not activated their account yet.
        include(drupal_get_path('module', 'discourse') . '/includes/discourse.inc');
        $data = array(
          'username' => $discourse_user['username'],
          'auth_provider' => 'Twitter',
          'awaiting_activation' => TRUE,
        );
      }
    }
    else {
      // User has not created an account yet. Mid-way through Twitter account creation.
      $data = array(
        'name' => $account->name,
        'auth_provider' => 'Twitter',
      );
      $data['username'] = discourse_get_discourse_username($data['name']);
      // Store the auth data so we can check later on account creation request.
      $_SESSION['discourse']['auth']['twitter']['account'] = $account;
    }
    discourse_auth_complete($data);
  }
  //@todo Deal with case where Twitter does not authenticate.
}


/**
 * Get the Twitter key and secret from the Discourse database.
 *
 * @return array
 */
function discourse_auth_twitter_get_config() {
  db_set_active('discourse');
  $twitter_key = db_select('site_settings', 'ss')
    ->fields('ss', array('value'))
    ->condition('name', 'twitter_consumer_key')
    ->execute()
    ->fetchField();
  $twitter_secret = db_select('site_settings', 'ss')
    ->fields('ss', array('value'))
    ->condition('name', 'twitter_consumer_secret')
    ->execute()
    ->fetchField();
  db_set_active('default');
  return array(
    'key' => $twitter_key,
    'secret' => $twitter_secret,
  );
}


/**
 * Twitter needs special treatment as we do not get a verified email.
 *
 * @params array $params
 *     POST data.
 */
function discourse_auth_twitter_create_user($params) {
  // Need to fake a password to get a user created by Discourse.
  $_POST['password'] = user_password();
  $response = discourse_ajax_response('/users');
  $json_data = json_decode($response->data);
  if ($json_data->success && isset($_SESSION['discourse']['auth']['twitter']['account'])) {
    $account = $_SESSION['discourse']['auth']['twitter']['account'];
    include(drupal_get_path('module', 'discourse') . '/includes/discourse_user.inc');
    $discourse_user = discourse_get_discourse_user_by_params(array('username' => $params['username']));
    // Unset the password and add a row to the twitter_user_infos table.
    $time = date('Y-m-d H:i:s.u');
    db_set_active('discourse');
    db_update('users')
      ->fields(array(
        'password_hash' => NULL,
        'salt' => NULL,
      ))
      ->condition('username', $discourse_user['username'])
      ->execute();
    db_insert('twitter_user_infos')
      ->fields(array(
        'user_id' => $discourse_user['id'],
        'screen_name' => $account->screen_name,
        'twitter_user_id' => $account->id,
        'created_at' => $time,
        'updated_at' => $time,
      ))
      ->execute();
    db_set_active('default');
    // We're done with session saved auth data now.
    unset($_SESSION['discourse']['auth']);
  }
  return $json_data;
}


/**
 * Return a Twitter authenticated Discourse user given their Twitter id.
 *
 * @param int $id
 *     Twitter user id
 * @return array|null
 */
function discourse_get_user_by_twitter_id($id) {
  db_set_active('discourse');
  $query = db_select('users', 'u');
  $query->join('twitter_user_infos', 'tui', 'tui.user_id = u.id');
  $query->fields('u');
  $query->condition('tui.twitter_user_id', $id);
  $discourse_user = $query->execute()->fetchAssoc();
  db_set_active('default');
  return $discourse_user;
}