<?php

/**
 * @file Handle Google OpenID external auth.
 */

include('common.inc');

/**
 * Setup Facebook auth redirect.
 */
function discourse_auth_facebook() {
  include(drupal_get_path('module', 'discourse') . '/lib/facebook/facebook.php');
  $config = discourse_auth_facebook_get_config();
  $facebook = new Facebook($config);
  if (isset($_GET['error'])) {
    discourse_auth_failure();
  }
  if (!count(discourse_get())) {
    discourse_auth_facebook_redirect($facebook);
  }
  else {
    discourse_auth_facebook_callback($facebook);
  }
}


/**
 * Perform Facebook auth redirect.
 */
function discourse_auth_facebook_redirect(Facebook $facebook) {
  // Need to make sure we have a session for the Facebook lib to work.
  if (!isset($_SESSION)) {
    session_start();
  }
  $location =  $facebook->getLoginUrl(array('scope' => 'email'));
  header('Location: ' . $location);
  exit;
}


/**
 * Handle Facebook auth callback.
 */
function discourse_auth_facebook_callback(Facebook $facebook) {
  include(drupal_get_path('module', 'discourse') . '/includes/discourse_user.inc');
  $user_profile = $facebook->api('/me','GET');
  $discourse_user = discourse_get_discourse_user_by_params(array('email' => $user_profile['email']));
  if ($discourse_user) {
    discourse_login($discourse_user);
    $data = array('authenticated' => TRUE);
  }
  else {
    $data = array(
      'email' => $user_profile['email'],
      'name' => $user_profile['name'],
      'email_valid' => TRUE,
      'auth_provider' => 'Facebook',
    );
    $data['username'] = discourse_get_discourse_username($data['name']);
    // Store the authed data so we can check later on account creation request.
    $_SESSION['discourse']['auth']['data'] = $data;
  }
  discourse_auth_complete($data);
}


/**
 * Get the Facebook app id and secret from the Discourse database.
 *
 * @return array
 */
function discourse_auth_facebook_get_config() {
  db_set_active('discourse');
  $fb_app_id = db_select('site_settings', 'ss')
    ->fields('ss', array('value'))
    ->condition('name', 'facebook_app_id')
    ->execute()
    ->fetchField();
  $fb_app_secret = db_select('site_settings', 'ss')
    ->fields('ss', array('value'))
    ->condition('name', 'facebook_app_secret')
    ->execute()
    ->fetchField();
  db_set_active('default');
  return array(
    'secret' => $fb_app_secret,
    'appId' => $fb_app_id,
  );
}
