<?php

	//custom url check
	function checkHttp($url) {
		if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
			$url = "http://" . $url;
		}
		return $url;
	}

/**

 * Implements hook_html_head_alter().

 * This will overwrite the default meta character type tag with HTML5 version.

 */

function thisgoodworld_html_head_alter(&$head_elements) {

  $head_elements['system_meta_content_type']['#attributes'] = array(

    'charset' => 'utf-8'

  );

}



/**

 * Override or insert variables into the page template for HTML output.

 */

function thisgoodworld_process_html(&$variables) {

  // Hook into color.module.

  if (module_exists('color')) {

    _color_html_alter($variables);

  }

}

/* Registration page hooks*/

function thisgoodworld_theme($existing, $type, $theme, $path){
  $hooks['user_register_form']=array(
    'render element'=>'form',
    'template' =>'templates/user-register',
  );
   $hooks['user_profile_form'] = array(
      'render element' => 'form',
      'template' => 'user-profile-form',
      'path' => drupal_get_path('theme', 'thisgoodworld') . '/templates',
   );

return $hooks;
}

function thisgoodworld_preprocess_user_register(&$variables) {
  $variables['form'] = drupal_build_form('user_register_form', user_register_form(array()));
}




function thisgoodworld_preprocess_user_profile_form(&$variables) {
  unset($variables['form']['group_account']['account']['mail']['#description']);
  $variables['form']['group_your_good']['field_for_the_environment']['und'][0]['value']['#title_display'] = 'invisible';
  $variables['form']['group_your_good']['field_for_your_community']['und'][0]['value']['#title_display'] = 'invisible';
  $variables['form']['group_your_good']['field_for_your_employees_culture']['und'][0]['value']['#title_display'] = 'invisible';
  $variables['form']['group_your_good']['field_for_social_change']['und'][0]['value']['#title_display'] = 'invisible';

  $form_inputs = array(
    'logo' => $variables['form']['group_account']['field_logo'],
    'mail' => $variables['form']['group_account']['account']['mail'],
    'name' => $variables['form']['group_account']['account']['name'],
    'field_facebook_url' => $variables['form']['group_online']['group_social']['field_facebook_url'],
    'field_website' => $variables['form']['group_online']['field_website'],
    'field_foursquare_url' => $variables['form']['group_online']['group_social']['field_foursquare_url'],
    'field_twitter_url' => $variables['form']['group_online']['group_social']['field_twitter_url'],
    'field_pinterest_url' => $variables['form']['group_online']['group_social']['field_pinterest_url'],
    'field_instagram_url' => $variables['form']['group_online']['group_social']['field_instagram_url'],
    'field_description' => $variables['form']['group_account']['field_description'],
    'field_for_your_community' => $variables['form']['group_your_good']['field_for_your_community'],
    'field_for_the_environment' => $variables['form']['group_your_good']['field_for_the_environment'],
    'field_for_social_change' => $variables['form']['group_your_good']['field_for_social_change'],
    'field_for_your_employees_culture' => $variables['form']['group_your_good']['field_for_your_employees_culture'],
    'account' => $variables['form']['group_account']['account'],
    'field_type_of_business' => $variables['form']['group_account']['field_type_of_business'],
    // 'field_type_of_good' => $variables['form']['group_account']['field_type_of_good'],
    'field_address' => $variables['form']['field_address'],
    'field_map_geofield' => $variables['form']['field_map_geofield'],
    'field_contact_email' => $variables['form']['field_contact_email'],
    'field_contact_email_2' => $variables['form']['field_contact_email_2'],
    'field_contact_email_3' => $variables['form']['field_contact_email_3'],
    'field_pledge' => $variables['form']['field_pledge'],
    'timezone' => $variables['form']['group_account']['timezone'],
    'picture' => $variables['form']['picture'],
    'actions' => $variables['form']['actions'],


  );

  $variables['rendered'] = _thisgoodworld_form_variables_render_all($form_inputs);
  $variables['username'] = $variables['form']['#user']->name;
  $variables['created'] = $variables['form']['#user']->created;

}

function _thisgoodworld_form_variables_render_all($elements) {
    //Create array to return, with element name as key and element as value
    $elements_array = array();
    //For each element, render it and add it to the array
    foreach ($elements as $key => $element) {
        $elements_array[$key] = render($element);
    }
    //Return array
    return $elements_array;
}

// hook_form_alter function
function thisgoodworld_form_alter(&$form, &$form_state, $form_id) {

    if ($form_id == 'user_profile_form') {
        $form['#after_build'][] = 'thisgoodworld_user_profile_form_after_build';
    }

    elseif($form_id == 'user_register_form'){
      if (function_exists('honeypot_add_form_protection')) {
        honeypot_add_form_protection($form, $form_state, array('honeypot', 'time_restriction'));
      }
    }
}

// afterbuild function
function thisgoodworld_user_profile_form_after_build($form) {
    // We want this on a specific field

    $form['field_for_the_environment']['und']['0']['format']['#access'] = FALSE;

    $form['field_for_social_change']['und']['0']['format']['#access'] = FALSE;
    $form['field_for_your_employees_culture']['und']['0']['format']['#access'] = FALSE;
    $form['field_for_your_community']['und']['0']['format']['#access'] = FALSE;
    return $form;
}

function thisgoodworld_preprocess_html(&$variables) {
  drupal_add_css('https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800|Oswald:400,300,700', array('type' => 'external'));
  $search_form = drupal_get_form('search_form');

$form = drupal_get_form('press_release_form');

//you now have a form array which can be themed or further altered...
$he = drupal_render($form);


  $variables['search_field'] =  $he;
}

function thisgoodworld_preprocess_page(&$variables) {
  $block = module_invoke('views', 'block_view', '-exp-map-page');
  $variables['filters'] = render($block['content']);
 if (arg(0) == 'news-room')
  drupal_add_css(path_to_theme() . '/css/blogs.css', array('group' => CSS_THEME, 'every_page' => FALSE));

}



/**

 * Override or insert variables into the page template.

 */

function thisgoodworld_process_page(&$variables) {

  // Always print the site name and slogan, but if they are toggled off, we'll

  // just hide them visually.

  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;

  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;

  if ($variables['hide_site_name']) {

    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.

    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));

  }

  if ($variables['hide_site_slogan']) {

    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.

    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));

  }

  // Since the title and the shortcut link are both block level elements,

  // positioning them next to each other is much simpler with a wrapper div.

  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {

    // Add a wrapper div using the title_prefix and title_suffix render elements.

    $variables['title_prefix']['shortcut_wrapper'] = array(

      '#markup' => '<div class="shortcut-wrapper clearfix">',

      '#weight' => 100,

    );

    $variables['title_suffix']['shortcut_wrapper'] = array(

      '#markup' => '</div>',

      '#weight' => -99,

    );

    // Make sure the shortcut link is the first item in title_suffix.

    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;

  }

  //switch social bar with as seen on section
  $activesection = variable_get('tgw_footer_section', 'social');
  if ($activesection == 'social'){
    $variables['social_footer'] = true;
       $variables['as_seen_on'] = false;
  }
  else{
    $variables['social_footer'] = false;
    $variables['as_seen_on'] = true;
  }
//compose as seen on logo section
 $logo1 = variable_get('logo1', false);
 $logo2 = variable_get('logo2', false);
 $logo3 = variable_get('logo3', false);
 $logo4 = variable_get('logo4', false);
 $i= 0;
 if($logo1){
  $i++;
  $url[$i] =file_load($logo1);
 }
 if($logo2){
  $i++;
  $url[$i] =file_load($logo2);
 }
 if($logo3){
  $i++;
  $url[$i] =file_load($logo3);
 }
 if($logo4){
  $i++;
  $url[$i] =file_load($logo4);
 }
 $dim = array(
    '1' => 'sixteen columns',
    '2' => 'eight columns',
    '3' => 'one-third column',
    '4' => 'four columns',
  );
 $j = $i;
 $asseenon_markup ='';
 while ( $j > 0) {
  if (isset($url[$j])){
    $asseenon_markup .= '<div class="'. $dim[$i] . ' "><img src="'. file_create_url($url[$j]->uri) .'" class="scale-with-grid" /></div>';
    $j--;
  }
 }

$variables['asseenon_markup'] = $asseenon_markup;


}



function thisgoodworld_page_alter($page) {

	// <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>

	$viewport = array(

		'#type' => 'html_tag',

		'#tag' => 'meta',

		'#attributes' => array(

		'name' =>  'viewport',

		'content' =>  'width=device-width, initial-scale=1, maximum-scale=1'

		)

	);

	drupal_add_html_head($viewport, 'viewport');

}



function thisgoodworld_breadcrumb($variables) {

  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {

    // Use CSS to hide titile .element-invisible.

    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    // comment below line to hide current page to breadcrumb

	$breadcrumb[] = drupal_get_title();

    $output .= '<div class="breadcrumb">' . implode('<span class="sep">»</span>', $breadcrumb) . '</div>';

    return $output;

  }

}



/**

 * Add Javascript for responsive mobile menu

 */

drupal_add_js(drupal_get_path('theme', 'thisgoodworld') .'/js/jquery.mobilemenu.js');



drupal_add_js('jQuery(document).ready(function($) {



$("#navigation .content > ul").mobileMenu({

	prependTo: "#navigation",

	combine: false,

	switchWidth: 768,

	topOptionText: "Select page"

});



});',

array('type' => 'inline', 'scope' => 'header'));

//EOF:Javascript
