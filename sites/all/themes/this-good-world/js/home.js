(function($) {
	$.fn.hoverIntent = function(handlerIn,handlerOut,selector) {

        // default configuration values
        var cfg = {
        	interval: 100,
        	sensitivity: 7,
        	timeout: 0
        };

        if ( typeof handlerIn === "object" ) {
        	cfg = $.extend(cfg, handlerIn );
        } else if ($.isFunction(handlerOut)) {
        	cfg = $.extend(cfg, { over: handlerIn, out: handlerOut, selector: selector } );
        } else {
        	cfg = $.extend(cfg, { over: handlerIn, out: handlerIn, selector: handlerOut } );
        }

        // instantiate variables
        // cX, cY = current X and Y position of mouse, updated by mousemove event
        // pX, pY = previous X and Y position of mouse, set by mouseover and polling interval
        var cX, cY, pX, pY;

        // A private function for getting mouse position
        var track = function(ev) {
        	cX = ev.pageX;
        	cY = ev.pageY;
        };

        // A private function for comparing current and previous mouse position
        var compare = function(ev,ob) {
        	ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
            // compare mouse positions to see if they've crossed the threshold
            if ( ( Math.abs(pX-cX) + Math.abs(pY-cY) ) < cfg.sensitivity ) {
            	$(ob).off("mousemove.hoverIntent",track);
                // set hoverIntent state to true (so mouseOut can be called)
                ob.hoverIntent_s = 1;
                return cfg.over.apply(ob,[ev]);
              } else {
                // set previous coordinates for next time
                pX = cX; pY = cY;
                // use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
                ob.hoverIntent_t = setTimeout( function(){compare(ev, ob);} , cfg.interval );
              }
            };

        // A private function for delaying the mouseOut function
        var delay = function(ev,ob) {
        	ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
        	ob.hoverIntent_s = 0;
        	return cfg.out.apply(ob,[ev]);
        };

        // A private function for handling mouse 'hovering'
        var handleHover = function(e) {
            // copy objects to be passed into t (required for event object to be passed in IE)
            var ev = jQuery.extend({},e);
            var ob = this;

            // cancel hoverIntent timer if it exists
            if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); }

            // if e.type == "mouseenter"
            if (e.type == "mouseenter") {
                // set "previous" X and Y position based on initial entry point
                pX = ev.pageX; pY = ev.pageY;
                // update "current" X and Y position based on mousemove
                $(ob).on("mousemove.hoverIntent",track);
                // start polling interval (self-calling timeout) to compare mouse coordinates over time
                if (ob.hoverIntent_s != 1) { ob.hoverIntent_t = setTimeout( function(){compare(ev,ob);} , cfg.interval );}

                // else e.type == "mouseleave"
              } else {
                // unbind expensive mousemove event
                $(ob).off("mousemove.hoverIntent",track);
                // if hoverIntent state is true, then call the mouseOut function after the specified delay
                if (ob.hoverIntent_s == 1) { ob.hoverIntent_t = setTimeout( function(){delay(ev,ob);} , cfg.timeout );}
              }
            };

        // listen for mouseenter and mouseleave
        return this.on({'mouseenter.hoverIntent':handleHover,'mouseleave.hoverIntent':handleHover}, cfg.selector);
      };
    })(jQuery);

    (function ($) {
    	Drupal.behaviors.mapload = {
    		attach: function (context, settings) {
    			var lMap = Drupal.settings.leaflet[0].lMap,
    					$geoisloaded = false;

                 var oms = new OverlappingMarkerSpiderfier(lMap, {keepSpiderfied:true});
                 var popup = new L.Popup();
                 oms.addListener('click', function(marker) {
                      popup.setContent(marker.desc);
                      popup.setLatLng(marker.getLatLng());

                });

                oms.addListener('spiderfy', function(markers) {
                  lMap.closePopup();
                });

                lMap.eachLayer(function(marker) {
                    if (marker._leaflet_id != 'earth' && marker._mRadius !== '500' && marker._latlng !== "undefined" && marker._content !== "You are here."){
                         oms.addMarker(marker);
                    }
                });

                //create thisgoodworld island
                var bounds = [[39.876303480607156, -71.81839942932129], [37.614524620667765, -68.91800880432129]];
                var island = L.rectangle(bounds,  {
                            color: 'red',
                            fillColor: '#eb6748',
                            fillOpacity: 0.5,

                            }).addTo(lMap);
                   var popup = L.popup({maxWidth: 100, className: 'island',offset: [20, 6] })
                               .setContent("This Good World island.");
                island.bindPopup(popup);
                // var popup = L.popup({maxWidth: 100, className: 'island',offset: [20, 6] })
                // .setLatLng(new L.LatLng(39.876303480607156, -71.81839942932129))
                // .setContent("This Good World island.")
                // .openOn(lMap);




    			if($('.leaflet-control-mapbox-geocoder').length > 0){
    				 // $geoisloaded = true;
    				  $('.leaflet-control-mapbox-geocoder').remove();
    			}

    			// lMap.addLayer(L.mapbox.tileLayer('thisgoodworld.map-hb8327xp'));
    		   if (!$geoisloaded){
    		   geosearch = L.mapbox.geocoderControl('thisgoodworld.map-hb8327xp', {'keepOpen' : true } );
    		   lMap.addControl(geosearch);
    		   $('.leaflet-control-mapbox-geocoder input').attr('placeholder', 'Search for good in your area...');
    		    var $geosearchfield = $('.leaflet-control-mapbox-geocoder');
    		    $('.views-exposed-form').before($geosearchfield);
    		    	  $(".leaflet-control-mapbox-geocoder input").blur(function(e){
					    		setTimeout(function(){$('.leaflet-control-mapbox-geocoder-results').hide();},1000);
					    	});
					    	$(".leaflet-control-mapbox-geocoder input").focus(function(e){
					    		setTimeout(function(){$('.leaflet-control-mapbox-geocoder-results').show();},100);

					    	});

    		 	}

    			 	lMap.on('move', function() {
    		 		 var $center = lMap.getCenter();
    		 		 var $zoom = lMap.getZoom();
    		 		 $.cookie('zoom', $zoom);
    		 		 $.cookie('lastposlat', $center.lat);
    		 		 $.cookie('lastposlon', $center.lng);
    		 		});

 						if($.cookie('lastposlat') !='undefined' && $('.views-exposed-form').hasClass('initialised-processed')){
 						 	 lMap.setView(new L.LatLng($.cookie('lastposlat'),$.cookie('lastposlon')), $.cookie('zoom'));
		 						var inBounds = [],
								boundd = lMap.getBounds();
								lMap.eachLayer(function(marker) {
								if (marker._leaflet_id != 'earth' && marker._mRadius !== '500' && marker._latlng !== "undefined" && marker._content !== "You are here."){
									if (boundd.contains(marker.getLatLng())) {
										inBounds.push(marker.getLatLng());
									}
								}
								});
								 // document.getElementById('undefined-sticky-wrapper').innerHTML = inBounds.join('\n');
								if (inBounds.length === 0) {
									CoverPop.open();
									}
 						}
 						//on the first load set the position
 						$('.views-exposed-form').once('initialised', function(){
    		 		 lMap.setView(new L.LatLng(40.737, -90.923), 4);
    		 		});
    		 		$('.zoomoutbtn').live('click',function(){
							lMap.zoomOut(2);
						});


    		  var features = [];

					geosearch.on('select', function(){
		 				$('.leaflet-control-mapbox-geocoder-results').show();
						var inBounds = [],
						boundd = lMap.getBounds();
						lMap.eachLayer(function(marker) {

						if (marker._leaflet_id != 'earth' && marker._mRadius !== '500' && marker._latlng !== "undefined" && marker._content !== "You are here."){
							if (typeof(marker.getLatLng) != "undefined") {

                                if (boundd.contains(marker.getLatLng())) {
                                inBounds.push(marker.getLatLng());
                                }

                            }
						}
						});
						 // document.getElementById('undefined-sticky-wrapper').innerHTML = inBounds.join('\n');
						if (inBounds.length === 0) {
							CoverPop.open();
							}
    		  });
					//center map on marker click
					// lMap.on('click', function(e){
					// 	alert(e.latlng);
					// });
					lMap.eachLayer(function(marker) {
						marker.on('click', function(e){
						lMap.setView(e.latlng);
						lMap.panBy([0, -200]);
						});
					});

						// lMap.on('move', function() {
						// var inBounds = [],
						// boundd = lMap.getBounds();
						// lMap.eachLayer(function(marker) {
						// if (marker._leaflet_id != 'earth' && marker._mRadius !== '500' && marker._latlng !== "undefined" && marker._content !== "You are here."){
						// 	if (boundd.contains(marker.getLatLng())) {
						// 		inBounds.push(marker.getLatLng());
						// 	}
						// }
						// });
						// // document.getElementById('undefined-sticky-wrapper').innerHTML = inBounds.join('\n');
						// if (inBounds.length === 0) {
						// 	CoverPop.open();
						// 	}
						// });
				}
    	};

   	Drupal.behaviors.checkcategories = {
    		attach: function (context, settings) {
    				//Uncomment to show subcategories on map filter
    			// $('.form-item-field-type-of-business-tid .form-type-bef-checkbox').hoverIntent(
    			// 	function(){
    			// 		 var $nextelem = $(this).next();
    			// 		 if ($nextelem.hasClass('bef-tree-child')){ $nextelem.show();}
    			// 	},function(){
    			// 	});
 	if (!Modernizr.touch){
    			 var $category_tag = $('.form-item-field-type-of-business-tid input');
    			 $category_tag.once('business-category', function() {
    			 $category_tag.click(function(){
    			 	$filtered = false;
    			 		$category_tag.each(function(){
    			 			if ($(this).prop('checked') ){
    			 				$filtered = true;
    			 				// console.log($filtered);
    			 			}
    			 		});
    			 		if ($filtered){
    			 			$('.views-widget-filter-field_type_of_business_tid > label').html('View all');
    			 		}
    			 		else{
    			 			$('.views-widget-filter-field_type_of_business_tid > label').html('Browse by <strong>CATEGORY</strong>');
    			 		}
    			 });

    		});
    		 	$('.views-widget-filter-field_type_of_business_tid > label').click(function(){
    			 		if ($('.views-widget-filter-field_type_of_business_tid > label').html() == 'View all'){

    			 			// $category_tag.prop('checked', false);
    			 				$category_tag.each(function(){
    			 					if ($(this).prop('checked') ){
    			 						$(this).attr('checked', false);
    			 						$('.views-widget-filter-field_type_of_business_tid').find('input[type=checkbox]:first').change();
    			 						$('.views-widget-filter-field_type_of_business_tid > label').html('Browse by <strong>CATEGORY</strong>');
    			 					}
    			 				});
    			 		}
    			 	});
    		 }
    		}//end Mordernizr
    		}//end behaviour

  	Drupal.behaviors.checkgoods = {
    		attach: function (context, settings) {
    				if (!Modernizr.touch){
    			 var $goods_tag = $('.form-item-field-type-of-good-tid input');
    			 $goods_tag.once('goods-category', function() {
    			 $goods_tag.click(function(){
    			 	$filtered = false;
    			 		$goods_tag.each(function(){
    			 			if ($(this).prop('checked') ){
    			 				$filtered = true;
    			 				// console.log($filtered);
    			 			}
    			 		});
    			 		if ($filtered){
    			 			$('.views-widget-filter-field_type_of_good_tid > label').html('View all');
    			 		}
    			 		else{
    			 			$('.views-widget-filter-field_type_of_good_tid > label').html('Browse by <strong>GOOD</strong>');
    			 		}
    			 });

    		});
    		 	$('.views-widget-filter-field_type_of_good_tid > label').click(function(){
    			 		if ($('.views-widget-filter-field_type_of_good_tid > label').html() == 'View all'){

    			 			// $category_tag.prop('checked', false);
    			 				$goods_tag.each(function(){
    			 					if ($(this).prop('checked') ){
    			 						$(this).attr('checked', false);
    			 						$('.views-widget-filter-field_type_of_good_tid').find('input[type=checkbox]:first').change();
    			 						$('.views-widget-filter-field_type_of_good_tid > label').html('Browse by <strong>GOOD</strong>');
    			 					}
    			 				});
    			 		}
    			 	});
    			}
    		}//end modernizr.touch
    		}//end behaviour

	Drupal.behaviors.geolocation = {
    		attach: function (context, settings) {
		//GEOLOCATION
			$('.leaflet-control-zoom').once('loaded',function(){
				var lMap = Drupal.settings.leaflet[0].lMap;
				$('.leaflet-control-zoom').append('<a class="leaflet-localise" href="#" title="Localise">Localise</a>')
				$('.leaflet-localise').click(function(){
					lMap.locate({setView: true, maxZoom: 12});
					lMap.on('locationfound',  onLocationFound);
				});
		function onLocationFound(e) {
		    // L.marker(e.latlng).addTo(lMap);
		    var circle = L.circle(e.latlng, 500, {
		    	color: 'red',
		    	fillColor: '#f03',
		    	fillOpacity: 0.5
		    }).addTo(lMap);
		    circle.bindPopup("You are here.");//.openPopup();
		    		//popup if not found
						var inBounds = [],
						boundd = lMap.getBounds();
						lMap.eachLayer(function(marker) {
						if (marker._leaflet_id != 'earth' && marker._mRadius !== 500 && marker._latlng !== "undefined" && marker._content !== "You are here."){
							if (typeof(marker.getLatLng) != "undefined") {
                                if (boundd.contains(marker.getLatLng())) {
	       							inBounds.push(marker.getLatLng());
			     				}
				            }
                		}
						});
						  // document.getElementById('undefined-sticky-wrapper').innerHTML = inBounds.join('\n');
						if (inBounds.length === 0) {
							CoverPop.open();
							}

		  }
		//END GEOLOCATION
			});
		}
	};
Drupal.behaviors.scrolltocity = {
    attach: function(context,settings){

    var cities = {
            'BOSTON, MA': {'lat' : 42.38783896858117, 'lon' : -71.08978271484375 } ,
            'BOULDER, CO': {'lat' : 40.036791855098706, 'lon' : -105.31717700986988 } ,
            'BURLINGTON, VT': {'lat' : 44.49294647889859, 'lon' : -73.22751709138423 } ,
            'CHICAGO, IL': {'lat' : 41.826980340293716, 'lon' : -87.69663891586538 } ,
            'DENVER, CO': {'lat' : 39.76922856217596, 'lon' : -104.86766139660075 } ,
            'DETROIT, MI': {'lat' : 42.32085478037569, 'lon' : -83.14949124401606 } ,
            'LOS ANGELES, CA': {'lat' : 34.03176514784702, 'lon' : -118.32139098312128 } ,
            'NEW YORK, NY': {'lat' : 40.78085513156505, 'lon' : -73.97893590843105 } ,
            'PHILADELPHIA, PA': {'lat' : 40.00263242220625, 'lon' : -75.11801429225599} ,
            'PORTLAND, ME': {'lat' : 43.67427562243075, 'lon' : -70.24693252747282 } ,
            'PORTLAND, OR': {'lat' : 45.64163072951364, 'lon' : -122.68514235321516 } ,
            'ROCHESTER, NY': {'lat' : 43.18887635901551, 'lon' : -77.65371899413631 } ,
            'SAN DIEGO, CA': {'lat' : 32.80359809421599, 'lon' : -117.05967184263734 } ,
            'SAN FRANCISCO, CA': {'lat' : 37.77030493019408, 'lon' : -122.43591332195355} ,

                };

    lMap = Drupal.settings.leaflet[0].lMap;
     $('.cities li').click(function(e){
      e.preventDefault();
     $('body').animate({scrollTop: 0 }, '500');
     var $city = ($(this).text().toUpperCase());
      lMap.setView(new L.LatLng(cities[$city].lat, cities[$city].lon), 12);
    });
    }
};

$(document).ready(function () {
    // $('#views-exposed-form-search-page div').removeClass('leaflet-control-mapbox-geocoder leaflet-bar leaflet-control-mapbox-geocoder-results' );
    // $('#views-exposed-form-search-page .leaflet-control-mapbox-geocoder-toggle').hide();

    $('.subscribe-now input[type="email"]').focus(function(){
       $('.subscribe-now .form-actions').fadeIn('fast');
    });

        //Jump to city on the map



            //play video
        $('#play-button').magnificPopup({
        items: {
            src: 'player.vimeo.com/video/82208780',
            type: 'iframe'
         }
        });
    		//center search form
				jQuery.fn.center = function () {
				    this.css("position","absolute");
				    // this.css("top", ( $(window).height() - this.height() ) / 2+$(window).scrollTop() + "px");
				    this.css("left", ( $(window).width() - this.width() ) / 2+$(window).scrollLeft() + "px");
				    return this;
				}
				$('#views-exposed-form-map-page').center();
				$(window).resize(function () { $('#views-exposed-form-map-page').center(); });

    		var lMap = Drupal.settings.leaflet[0].lMap;
    		// lMap.addControl(L.mapbox.geocoderControl('thisgoodworld.map-hb8327xp'));

    		$("label").inFieldLabels();
    		//first time popup
    		CoverPop.start({
    			expires: 1
    		});

    		$('.frstvisit').click(function(){
    			$('.splash-center').html("<p class='large-text'>LOOKS LIKE WE’RE NOT THERE...YET.</p> \
	       		<div class='search-now orange zoomoutbtn CoverPop-close frstvisit'>KEEP DISCOVERING</div> \
	       		<div class='btn-wrapper'><a href='contact' class='btn keep'>KEEP IN TOUCH</a><a href='members' class='btn citys'>CURRENT CITIES</a></div>");
    		});

				if (checkforcookie('_CoverPop')){
					$('.splash-center').html("<p class='large-text'>LOOKS LIKE WE’RE NOT THERE...YET.</p> \
	       		<div class='search-now orange zoomoutbtn CoverPop-close frstvisit'>KEEP DISCOVERING</div> \
	       		<div class='btn-wrapper'><a href='contact' class='btn keep'>KEEP IN TOUCH</a><a href='members' class='btn citys'>CURRENT CITIES</a></div>");
					}
				$('.CoverPop-close').live('click',function(){CoverPop.close();});

		if (Modernizr.touch) {

				var $categ_dropdown = $('#edit-field-type-of-business-tid-wrapper .views-widget');
					$categ_dropdown.hide();
				var $good_dropdown = $('#edit-field-type-of-good-tid-wrapper .views-widget');
					$good_dropdown.hide();
		    $('#edit-field-type-of-business-tid-wrapper > label').on({ 'touchstart' : function(){
		    		$categ_dropdown.slideToggle();
						$('.form-item-field-type-of-business-tid ul li ul.bef-tree-child').hide();
		    	 } });
	      $('#edit-field-type-of-good-tid-wrapper > label').on({ 'touchstart' : function(){
	    		$good_dropdown.slideToggle();
					 } });
	   		$('#supersized').on('touchstart', function () {
					$categ_dropdown.hide();
					$good_dropdown.hide();
					});
		} else {
			var $categ_dropdown = $('#edit-field-type-of-business-tid-wrapper .views-widget');
			$categ_dropdown.hide();
			$("#edit-field-type-of-business-tid-wrapper").hoverIntent(
				function(){
					$categ_dropdown.slideToggle();
					$('.form-item-field-type-of-business-tid ul li ul.bef-tree-child').hide();
				},
				function(){
					$categ_dropdown.slideToggle();
				}
			);

			var $good_dropdown = $('#edit-field-type-of-good-tid-wrapper .views-widget');
			$good_dropdown.hide();
			$("#edit-field-type-of-good-tid-wrapper").hoverIntent(
				function(){
					$good_dropdown.slideToggle();
				},
				function(){
					$good_dropdown.slideToggle();
				}
			);
		}

		// var lMap = Drupal.settings.leaflet[0].lMap;
		// lMap.addControl(L.mapbox.geocoderControl('thisgoodworld.map-hb8327xp'));
		// lMap.addLayer(L.mapbox.tileLayer('thisgoodworld.map-hb8327xp'));
		// lMap.setView(new L.LatLng(40.737, -90.923), 4);

		$(".view-good-blog-latest img").addClass("scale-with-grid");

		//===Featured members===
		numSets = 3;
        first = 1;// Math.floor(Math.random()*(numSets));
		current = first;
		looped = 0;
		numLoops = 6;
		i = 0;
		fIn = 300;
		fOut = 200;
		delay = 0;
		$(".set-" + current).fadeIn(fIn);
		showNext();

		function showNext() {
			$(".set-" + current).delay(4000).fadeOut(fOut, function() {
				i++;

				if (i >= 4) {
					if (current < numSets) {
						current++;
					}
					else if (current == numSets) {
						current = 1;
					}

                        $(".set-" + current).each(function(){

                        $(this).delay(delay).fadeIn(fIn);
                        delay += 500;
                     });

					looped++;

					if (looped < numLoops) {
						showNext();
					}
					// delay += 500;
					i = 0;
				}
			});

		}

		$(".blog-section-header.active").hover(
			function() {
				$(this).stop().animate({
					"background-position-y": "100%"
				}, 200, "easeInSine")
			}, function() {
				$(this).stop().animate({
					"background-position-y": "0%"
				}, 200, "easeOutSine")
			}
			);
	});

})(jQuery);

function loadProfile(pID) {
	jQuery("#profile-container").load( "user/" + pID + " #user-profile", function() {
		jQuery('.profile-description .field-item, .area-of-good .field-item').readmore({
			speed: 600,
			maxHeight: 165,
			moreLink: '<a href="#">Read more</a>',
			lessLink: '<a href="#">Collapse</a>'
		});
	});
	scrollToAnchor('profile-container');
}

function scrollToAnchor(aid){
	var aTag = jQuery("#" + aid);
	jQuery('html,body').animate({scrollTop: aTag.offset().top-74},'slow');
}

function showVideo(thumbnail, videoID) {
	vbase = "//player.vimeo.com/video/"
	jQuery(".video").attr("src", vbase + videoID);
	jQuery(".video-thumbnail").removeClass("selected");
	jQuery(thumbnail).addClass("selected");
	scrollToAnchor('video-player');
}
function checkforcookie(name){
  if (document.cookie.indexOf(name) !== -1) {

     return true;
  }
    return false;
  }

