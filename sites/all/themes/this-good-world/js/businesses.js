(function ($) {
	$(document).ready(function () {
		$(".question").click(function() {
			$(this).parent().find(".answer").toggle("blind", {direction: "vertical"}, "slow");
		});

//smooth scrolling
    $('a[href^="#"]').click(function (e) {

      e.preventDefault();
      var target = this.hash,
      $target = $(target);
      var $d = $target.offset().top - 150;
      $('html, body').stop().animate({
          'scrollTop': $d
      }, 900, 'swing', function () {
          window.location.hash = target;
      });
  });

//sticky menu
  var starting_position = $('.title-bar').outerHeight( true ) + $('.business-menu').outerHeight( true ) + 150;
   $(window).scroll(function() {
    var yPos = ( $(window).scrollTop() );
    if( yPos > starting_position && window.innerWidth > 500 ) { // show sticky menu after these many (starting_position) pixels have been scrolled down from the top and only when viewport width is greater than 500px.
      $(".sticky-business-menu").fadeIn();
    } else {
      $(".sticky-business-menu").fadeOut();
    }
  });



});
})(jQuery);
