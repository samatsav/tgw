(function ($) {
	$(document).ready(function () {
		
		var small_handler = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(12500);
				var $plan_id = $('<input type=hidden name=planId />').val("halfsmall");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);      			
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('halfsmall').addEventListener('click', function(e) {
			// Open Checkout with further options
			small_handler.open({
				name: 'This Good World',
				description: 'Half Off Small Plan',
				currency: "usd",
				interval: "year",
				trial_period_days: "0",
				amount: 12500
			});
			e.preventDefault();
		});
		
		var small_handler_monthly = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(1250);
				var $plan_id = $('<input type=hidden name=planId />').val("halfsmallmonth");
				var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('halfsmallmonth').addEventListener('click', function(e) {
			// Open Checkout with further options
			small_handler_monthly.open({
				name: 'This Good World',
				description: 'Half Off Monthly Small',
				currency: "usd",
				interval: "month",
				trial_period_days: "0",
				amount: 1250
			});
			e.preventDefault();
		});
		
		var medium_handler = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(50000);
				var $plan_id = $('<input type=hidden name=planId />').val("halfmedium");
       			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);     			
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
      		}
		});
	
		document.getElementById('halfmedium').addEventListener('click', function(e) {
			// Open Checkout with further options
			medium_handler.open({
				name: 'This Good World',
				description: 'Half Off Medium Plan',
				currency: "usd",
				interval: "year",
				trial_period_days: "0",
				amount: 50000
			});
			e.preventDefault();
		});
		
		var medium_handler_monthly = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(4750);
				var $plan_id = $('<input type=hidden name=planId />').val("halfmediummonth");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);      			
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
      		}
		});
	
		document.getElementById('halfmediummonth').addEventListener('click', function(e) {
			// Open Checkout with further options
			medium_handler_monthly.open({
				name: 'This Good World',
				description: 'Half Off Monthly Medium',
				currency: "usd",
				interval: "month",
				trial_period_days: "0",
				amount: 4750
			});
			e.preventDefault();
		});
		
		var large_handler = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(125000);
				var $plan_id = $('<input type=hidden name=planId />').val("halflarge");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);      			
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
      		}
		});
	
		document.getElementById('halflarge').addEventListener('click', function(e) {
			// Open Checkout with further options
			large_handler.open({
				name: 'This Good World',
				description: 'Half Off Large Plan',
				currency: "usd",
				interval: "year",
				trial_period_days: "0",
				amount: 125000
			});
			e.preventDefault();
		});
		
		var large_handler_monthly = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(11250);
				var $plan_id = $('<input type=hidden name=planId />').val("halflargemonth");
       			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);     			
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
      		}
		});
	
		document.getElementById('halflargemonth').addEventListener('click', function(e) {
			// Open Checkout with further options
			large_handler_monthly.open({
				name: 'This Good World',
				description: 'Half Off Monthly Large',
				currency: "usd",
				interval: "month",
				trial_period_days: "0",
				amount: 11250
			});
			e.preventDefault();
		});
		
		
	});
})(jQuery);