(function ($) {
	$(document).ready(function () {
		$('#edit-field-description-und-0-value').keyup(function () {
				var left = 400 - $(this).val().length;
				if (left < 0) {
						left = 0;
				}
				$('#counter').text(left);
		});
		
		$(".social li:nth-child(" + 1 + ")").addClass("active");
		$(".social li:nth-child(" + 1 + ")").append("<span class='arrow'></span>");
		$(".social .field-type-text:nth-child(" + 1 + ")").show();
		
		$("#field-contact-email-values tr").removeClass("draggable odd");
		
		$(".social li").click(function() {
			var ord = $(".social li").index(this);
			$(".social .field-type-text").hide();
			$(".social .field-type-text:nth-child(" + (ord+1) + ")").fadeIn();
			
			$(".social li .arrow").remove();
			$(this).append("<span class='arrow'></span>");
			
			$(".social li").removeClass("active");
			$(this).addClass("active");
			//var sUrl = $(".social .field-type-text:nth-child(" + (ord+1) + ") input").val();
			//$(this).children("label").prepend("<i class='icon-ok'></i>");
		});
		
		function validateURL(textval) {
			var urlregex = new RegExp(
						"^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
			return urlregex.test(textval);
		}
		
	});
})(jQuery);