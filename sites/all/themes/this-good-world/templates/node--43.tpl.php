
	<div class="container">
		<div class="sixteen columns title-bar">
			<h1>About</h1>
			<span>What is This Good World?</span>
		</div>
			
		<div class="sixteen columns" id="video-player">
			<iframe src="//player.vimeo.com/video/82208779" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen class="video"></iframe>
		</div>							
			
	</div>
		
		<div class="container page-section">

			<div class="eight columns">
				<h3>Discover + Support</h3>
				<p>This Good World is a search + discovery platform that highlights and supports businesses doing <em>good</em> things.
<p>
We believe good comes in all shapes and sizes, all of which is important. Whether a business is active with environmental issues, promoting social change, bringing positive action to the community, doing great things for their employees or all of the above, This Good World lets people know about it.</p>

			</div>

			<div class="eight columns">
				<h3>Community</h3>
<p>Our community consists of <em>conscious consumers, contributors, ambassadors, our members, partners (associations, chambers, tourism groups, schools), for profit, non profit and staff</em>–in your hometown and beyond.</p>

				<p>"It really captures what we believe in as far as using the community to connect with people that have the same kind of objectives that we do." -Tom Kehoe, owner and brewmaster of <a href=“http://dev.thisgoodworld.com/yardsbrewingcompany”>Yards Brewing Company.</a></p>
				
			</div>
		</div>

		<div class="container sixteen">
			<div class="call-out">Find out how you can <a href="/getinvolved">get involved</a> or <a href="/contact">contact us</a>.</div>	
		</div>

		<div class="container sixteen page-section">
			<a href="https://thisgoodworld.com/join" class="join-button">
				<img src="<?php echo path_to_theme(); ?>/images/businesses-join.png" alt="Business join here" />
			</a>	
		</div>

		<!-- Team -->
		
		<div class="container page-section">
		
			<div class="sixteen columns">
				<h2>The Team</h2>
				<ul class="team">

					<li class="four columns alpha">
						<img src="<?php echo path_to_theme(); ?>/images/lisa-kribs-lapierre-thisgoodworld.jpg" title="Lisa 'I'm With The Band' Kribs-LaPierre - Co-Founder" class="scale-with-grid">
						<div class="info">
							<h4>
								<span class="color">Lisa</span>Kribs-LaPierre</h4>
							<em>Co-Founder</em>
						</div>
						<div class="social">
							<a href="http://twitter.com/lisakribs" target="_blank">
								<i class="icon-twitter"></i>
							</a>
						</div>
					</li>

					<li class="four columns">
						<img src="<?php echo path_to_theme(); ?>/images/gavin-thomas-thisgoodworld.jpg" title="Gavin 'Never Gonna Be Bald' Thomas - Co-Founder" class="scale-with-grid">
						<div class="info">
							<h4>
								<span class="color">Gavin</span>Thomas</h4>
							<em>Co-Founder</em>
						</div>
						<div class="social">
							<a href="http://twitter.com/gavth" target="_blank">
								<i class="icon-twitter"></i>
							</a>
						</div>
					</li>

					<li class="four columns">
						<img src="<?php echo path_to_theme(); ?>/images/andy-ballerstein-thisgoodworld.jpg" title="Andy 'This Is My Nicest T-Shirt' Ballerstein - Lead Developer" class="scale-with-grid">
						<div class="info">
							<h4>
								<span class="color">Andy</span>Ballerstein</h4>
							<em>Lead Developer</em>
						</div>
						<div class="social">
							<a href="http://twitter.com/andyballerstein" target="_blank">
								<i class="icon-twitter"></i>
							</a>
						</div>
					</li>

					<li class="four columns omega">
						<img src="<?php echo path_to_theme(); ?>/images/katie-baldwin-thisgoodworld.jpg" title="Katie 'Picture Perfect' Baldwin - Director of Membership" class="scale-with-grid">
						<div class="info">
							<h4>
								<span class="color">Katie</span>Baldwin</h4>
							<em>Director of Membership</em>
						</div>
						<div class="social">
							<a href="http://twitter.com/katiembaldwin" target="_blank">
								<i class="icon-twitter"></i>
							</a>
						</div>
					</li>

				</ul>
			</div>
		</div>
		
		<!-- ./Team -->