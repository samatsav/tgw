<div id="wrap">

    <section class="menu">
      <div class="container">
    
        <div class="sixteen columns clearfix">
          
          <h1 class="logo"><a href="/"><img src="/assets/img/logo-footer.png" width="299" height="40" alt="This Good World. Copyright 2013. All rights reserved." /></a></h1>
    
    
    
          <nav class="menu-holder">
            <ul id="main-menu" class="menu-list">
              <li><a href="/#about-us">About Us<span class="arrow"></span></a></li>
              <li><a href="/#the-scoop">The Scoop<span class="arrow"></span></a></li>
              <li><a href="/#what-you-get">What You Get<span class="arrow"></span></a></li>
              <li><a href="/#contact">Contact<span class="arrow"></span></a></li>
              <li><a href="http://isgoodblog.tumblr.com/" target="_blank">Is Good<span class="arrow"></span></a></li>
              <li class="active"><a href="user/register">Join<span class="arrow"></span></a></li>          
            </ul>
          
            <!-- SubMenu -->
    
            <select size="1" id="subMenu" class="sub-menu" name="subMenu" onChange="moveTo(this.value)">
              <option selected="selected" value="">Menu</option>
              <option value="#about-us">- About Us</option>
              <option value="#the-scoop">- The Scoop</option> 
              <option value="#what-you-get">- What You Get</option>
              <option value="#contact">- Contact</option>
              <option value="http://isgoodblog.tumblr.com/">- Is Good</option>
              <option value="">- Join Us</option> 
            </select>
          </nav>
    
          
    
        </div> <!-- ./sixteen columns -->
    
    
      </div> <!-- ./container -->
    </section> <!-- ./menu -->

    <div class="container">

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--></div><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8" />
	<title>The Scoop - This Good World</title>
	<meta name="description" content="What This Good World is all about" />
	<meta name="keywords" content="This Good World" />

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

		
		<!-- CSS
  ================================================== -->
  	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,800|Oswald:400,300,700' rel='stylesheet' type='text/css' />
  	<link rel="stylesheet" href="assets/css/supersized.css" />
  	<link rel="stylesheet" href="assets/css/supersized.shutter.css" />
	<link rel="stylesheet" href="assets/css/base.css" />
	<link rel="stylesheet" href="assets/css/font-awesome.css" />
	<link rel="stylesheet" href="assets/css/skeleton.css" />
	<link rel="stylesheet" href="assets/css/layout.css" />
	<link rel="stylesheet" href="assets/css/prettyPhoto.css" />

	<!-- COLOR CSS -->
	<link id="default" rel="stylesheet" href="assets/css/tgw-brand.css" />


	<!-- REVOLUTION BANNER CSS SETTINGS -->
	<link rel="stylesheet" type="text/css" href="assets/rs-plugin/css/settings.css" media="screen" />

	<!-- get jQuery from the google apis -->
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.js"></script>


	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/font-awesome-ie7.css">
		<style type="text/css">
			
			.page-wrapper,.footer{
				width: 100%;
				position: relative;
			}
			#break3{
				overflow: hidden;
			}
			
		</style>
	<![endif]-->

</head>
<body>
<!-- Primary Page Layout
	================================================== -->
<script type="text/javascript">
    	 function moveTo(contentArea){
        	var goPosition = $(contentArea).offset().top - 73;
        	$('html,body').animate({ scrollTop: goPosition}, 'slow');
        }
   // </script>
<div id="up"></div>



<section class="home">
	<div class="overlay">
		<div class="super"> 

			<div class="slider-container">
     		<h4>Friends, This Good Map is processing&hellip; be the first to know when it's ready.</h4>
				<div class="contact-area" style="clear: both;">
					<form action="php/form.php" method="post" id="contactForm">
						<label for="cemail"></label>
						<input type="text" id="cemail" name="email" placeholder="Email" autocapitalize="off" class="required email" />
						<button class="std-button" type="submit">Sign Up</button>
             <br class="clear" />
					</form>
					<div class="successMessage"></div>	
				</div>  
        <hr />
        <h5>Are you an organization looking to spread some good? Join our Founding Member Beta today and we'll pick up the tab.</h5>      
        <a href="/join"><button class="std-button" type="submit" id="join-button">Join</button></a>
			</div>

		</div><!-- ./super -->
	</div> <!-- ./overlay -->
</section> <!-- ./home -->



<section class="menu">
	<div class="container">

		<div class="sixteen columns clearfix">
			
			<h1 class="logo"><a href="#up"><img src="/assets/img/logo-footer.png" width="299" height="40" alt="This Good World. Copyright 2013. All rights reserved." /></a></h1>



			<nav class="menu-holder">
				<ul id="main-menu" class="menu-list">
					<li class="active"><a href="#about-us">About Us<span class="arrow"></span></a></li>
					<li><a href="#the-scoop">The Scoop<span class="arrow"></span></a></li>
          <li><a href="#what-you-get">What You Get<span class="arrow"></span></a></li>
          <li><a href="#contact">Contact<span class="arrow"></span></a></li>
					<li><a href="isgood" target="_blank">Is Good<span class="arrow"></span></a></li>
          <li><a href="join">Join<span class="arrow"></span></a></li>          
				</ul>
			
				<!-- SubMenu -->

				<select size="1" id="subMenu" class="sub-menu" name="subMenu" onChange="moveTo(this.value)">
					<option selected="selected" value="">Menu</option>
					<option value="#about-us">- About Us</option>
					<option value="#the-scoop">- The Scoop</option> 
					<option value="#what-you-get">- What You Get</option>
          <option value="#contact">- Contact</option>
          <option value="http://isgoodblog.tumblr.com/">- Is Good</option>
					<option value="">- Join Us</option> 
				</select>
			</nav>

			

		</div> <!-- ./sixteen columns -->


	</div> <!-- ./container -->
</section> <!-- ./menu -->



<div class="page-wrapper" id="container">

<a name="about-us" id="about-us" class="menu-offset"></a>

<section class="about">

		<div class="container">


			<!-- SECTION TITLE -->

			<div class="sixteen columns title-bar">
				<h1>About Us</h1>
				<span>Why This Good World?</span>
			</div>



			<div class="sixteen columns">

				<!-- MESSAGE -->

				<h2 class="message-block">
					<iframe src="http://player.vimeo.com/video/66238584" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen id="video"></iframe>
				</h2>

				<!-- END MESSAGE -->


			</div> <!-- ./sixteen columns -->
		</div> <!-- ./container -->


		<div class="container">

			<div class="eight columns">
				<h3>It used to be…</h3>
				<p>A community of businesses and professionals came together and met in one room, voicing opinions, taking dues, sharing values and talking about service. Today, with this big world at each of our fingertips, that original, authentic way of being is more important than ever… we're talking peace and goodwill. This Good World, just like back in the day, and just like home.</p>
				<p>This Good World is a gathering of like minds, coming together to better the earth. We are a network. We are a community. We are committed to finding, spreading and creating good.</p>
			</div>

			<div class="eight columns">
	      <h3>Our mission</h3>
        <p>This Good World is a conduit for collaboration and action within digital neighborhoods comprised of good organizations and those looking to support them. Together, we are like minds conscious of what truly matters.</p>
				<p><em>If good exists, we will find it. <br />When good is found, we will spread it. <br />If good is not yet there, we will create it.</em></p>
			</div>

		</div> <!-- ./container -->


</section> <!-- ./about -->



<!-- PARALLAX -->

<section class="para" id="break1">
	<div class="overlay">
		<div class="container message">
			<p>If good exists, we will find it...</p>
		</div>
	</div>
</section>


<!-- END PARALLAX -->

<a name="the-scoop" id="the-scoop" class="menu-offset"></a>

<section class="about">

		<div class="container">


			<!-- SECTION TITLE -->

			<div class="sixteen columns title-bar">
				<h1>The Scoop</h1>
				<span>What is This Good World?</span>
			</div>

		</div> <!-- ./container -->

		<div class="container">

			<div class="sixteen columns">
				<h3>Background</h3>
				<p>So you're a good business doing good things. It's important for you and your employees to play a part in the bigger picture. No matter your size, where do you tell folks about the good you're committed to doing? Your website? Sure. FB wall? Sure ok. Tweet it? Ok for some. But what about a community of other like minded businesses – both local and national, all on the same page trying to do the same thing?</p>
				<h2 class="message-block">Coming together to get good done. Committed to one common pledge.</h2>
        <img src="/assets/img/sign.png" width="150" height="150" alt="" align="right" id="sign" />
        <p>This Good World is a new collaboration platform aimed at making it easy for businesses to propose issues and solutions or actions that lead to more good in their areas.</p>
        <p>It's your new home for sharing ideas and service, leveraging the power of the pledge and learning from one another. The best part is like minded consumers call This Good World home too. They have a front row seat to watch all the good you and your fellow good businesses are doing. A front row seat with their hands on their wallets – ready to support.</p>
        <p><strong>Here's how it all plays out…</strong></p>
			</div>

		</div> <!-- ./container -->
    
    <div class="container">
    	<div class="sixteen columns">
      	<h3>The Neighborhood</h3>
        <p>This Good World is an open membership platform. Any business can join from any location at any time. The first organization to join from any given city or town designates a shiny new location marker on the map and a brand spankin' new neighborhood page for their community – soon to be chock-full-o' good businesses by their side. The very first business is well on its way to being the hero – the one who knocked on the door leading into a whole new possibility of good for their local neighborhood.</p>
				<p>It's true what they say – it only takes one to truly change the world. However, at its core, This Good World is about collaboration. It's about businesses coming together and acting as that one force behind real change. That's what happens in each local forum.</p>
				<p>In order for truly impactful and actionable dialogue to occur in these forums, a critical mass of twelve businesses in any given location must join for that digital town hall forum to become unlocked. This isn't a place for crickets, bird chirps and dust bunnies rollin' by. This is place for problem solving, cooperation and real, solid, wholesome good.</p>
      </div>       
    	<div class="eight columns">
      	<h3>The Profile</h3>
				<div class="img-frame">
						<a href="<?php print path_to_theme(); ?>/images/profile.png"  alt="Sample profile for Joe Bean Coffee Roasters" class="prettyPhoto">
							<img src="/assets/img/profile-thumbnail.png" alt="" class="scale-with-grid illustration" />
							<span class="img-over"></span>
							<img class="icon" src="/assets/img/theme/magnify.png" alt="" />
						</a>
				</div>     
				<p>This is your place to shine. A place to not only share your contact information and voice your commitment to doing good, but to also share the good you already do on a regular basis. Upon joining, you'll fill out a form identifying the good things that are meaningful to your business. Donate 5% of your profits to a local charity? Great. Encourage high fives in the office as much as possible? Awesome. Have a company garden that was once unused green space out back? Sweet. Big good, small good and any size good in between. We want these to be your identifiers (and so do your customers). In order to help the search and sort function here, we've broken potential categories into 4 groups: Community, Environment, Social and Employees/Culture.</p>
      </div>
    	<div class="eight columns">
      	<h3>This Good Map</h3>
        <img src="/assets/img/map.png" width="460" height="300" alt="" class="illustration" />
        <p>A searchable directory, by location and category, of all businesses participating in This Good World. As a member, you now have a spot on this map – to see and be seen.</p>
				<p>Whether local or in from out of town, good people wanting to spend their dollars with good businesses can use this map to satisfy that desire. Businesses can also use this map to foster a local network of support back and forth.</p>
      </div>   
    </div>


</section> <!-- ./the-scoop -->


<!-- PARALLAX -->

<section class="para" id="break2">
	<div class="overlay">
		<div class="container message">
			<p>When good is found, we will spread it...</p>
		</div>
	</div>
</section>

<!-- END PARALLAX -->

<a name="what-you-get" id="what-you-get" class="menu-offset"></a>

<section class="about" id="what-you-get-section">

		<div class="container">


			<!-- SECTION TITLE -->

			<div class="sixteen columns title-bar">
				<h1>What You Get</h1>
				<span>Why should businesses join?</span>
			</div>

		</div> <!-- ./container -->

		<div class="container">

			<div class="eight columns">
				<h3>A Seat in the Hall.</h3>
				<p>Membership into the This Good World Network – a collaborative of businesses committed to bringing good to their area. All members get an equal spot on the map regardless of size, structure or industry.</p>
				<h3>A Voice.</h3>
				<p>Access to your local forum – a place for sharing ideas, service, pledges and bringing forth topics that are meaningful to you and your organization.</p>
				<h3>A Resource.</h3>
				<p>Collaboration opportunity with other good businesses on the map, both near and far. Notice some good being done elsewhere? Think you can bring that back to your business? Yeah, we think so too.</p>
				<h3>A Good Profile.</h3>
        <p>A spot to tell the world about your commitment to good – past, present and future. Show good consumers that you care about what they care about and they'll come flocking to your doors.</p>              
			</div>
      
      <div class="eight columns">
        <h3>A Good Outlet.</h3>
        <p>Your time is important, so is tweeting and keeping your online presence fresh. Now you have a place, a new kind of network that connects you directly with like minded folks. This is a place for businesses who like to do good, want to do good, need to do good. Nothing more. Nothing Less.</p>
        <h3>A Good Package.</h3>
        <p>A collection of digital materials to help display your commitment to good.</p><p>
        </p><ul class="bulleted">
        	<li>Membership Logo</li>
          <li>Digital pledge card (for use in ABOUT section, etc)</li>
          <li>Membership announcement (Spotlight Feature) on This Good World social properties (with a link to your shiny new profile)</li>
          <li>1 Customizable blog post for your site (extra do-gooders may even get featured on the "Is Good" section of our site)</li>
        </ul>
       
      </div>

		</div> <!-- ./container -->
    
    <div class="container">
			<div class="sixteen columns">      
      
        <!-- MESSAGE -->
        
        <h2 class="message-block">
          Sound good? This will sound even better - we're currently passing out free lifetime memberships to exclusive founding members. <a href="/join">Click here</a> to become one now.
        </h2>
        
        <!-- END MESSAGE -->
      
      </div>    
    </div>
    
            


</section> <!-- ./what-you-get -->


<!-- PARALLAX -->

<section class="para" id="break3">
	<div class="overlay">
		<div class="container message">
			<p>If good is not yet there, we will create it.</p>
		</div>
	</div>
</section>

<!-- END PARALLAX -->

<a name="contact" id="contact" class="menu-offset"></a>

<section class="contact">

		<div class="container">

			<!-- SECTION TITLE -->

			<div class="sixteen columns title-bar">
				<h1>Contact</h1>
			</div>

		</div> <!-- ./container -->

		<div class="container panel-wrap">

			<!-- TEXT AREA -->
			<div class="one-third column">
				<h3>About This Good World</h3>
				<p>This Good World is a conduit for collaboration and action within digital neighborhoods comprised of good organizations and those looking to support them. Together, we are like minds conscious of what truly matters. </p>

			</div>

			 <!-- FEATURE LIST -->

			<div class="one-third column">
				<h3>Get In Touch</h3>
				<ul class="list" id="contact">
					<a href="mailto:contact@thisgoodworld.com" target="_blank"><li class="first"><i class="icon-envelope-alt"></i>Email</li></a>
					<a href="http://www.facebook.com/thisgoodworld" target="_blank"><li><i class="icon-facebook"></i>Facebook</li></a>
					<a href="http://www.twitter.com/thisgoodworld" target="_blank"><li><i class="icon-twitter"></i>Twitter</li></a>
					<a href="http://www.instagram.com/thisgoodworld" target="_blank"><li><i class="icon-camera"></i>Instagram</li></a>
					<a href="https://foursquare.com/thisgoodworld" target="_blank"><li class="last"><i class="icon-globe"></i>Foursquare</li></a>
				</ul>        
			</div>

			<!-- Flickr -->

			<div class="one-third column">
      	<h3>The Pledge</h3>
				<p>If good exists, we will find it. <br />When good is found, we will spread it. <br />If good is not yet there, we will create it.</p>
			</div>

		</div> <!-- ./container -->
     
</section> <!-- ./contact -->

  <section id="footer">
    <div class="container">
      <div class="sixteen columns">
        <h2 class="message-block">
          Questions? Comments? Just wanna chat? <a href="mailto:contact@thisgoodworld.com">contact@thisgoodworld.com</a>
        </h2>
      </div> 
      <div class="sixteen columns">
        <a href="privacy-policy" id="privacy-policy">Privacy Policy</a>    
      </div>
    </div>        
       
  </section>    


</div> <!-- ./page-wrapper -->

<!-- End Document
================================================== -->




<!-- JS
================================================== -->


<script type="text/javascript" src="http://flesler-plugins.googlecode.com/files/jquery.scrollTo-1.4.2-min.js"></script>
<script type="text/javascript" src="http://flesler-plugins.googlecode.com/files/jquery.localscroll-1.2.7-min.js"></script>
<script type="text/javascript" src="/assets/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="/assets/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="/assets/js/jquery.form.js"></script>
<script src="/assets/js/jquery.validate.min.js"></script>
<script src="/assets/js/scripts.js"></script>
<script src="/assets/js/supersized.3.2.7.js"></script>
<script src="/assets/js/supersized.shutter.min.js"></script>
<script src="/assets/js/jquery.prettyPhoto.js"></script>
<script src="/sites/all/themes/this-good-world/js/sizzle-main.js"></script>    

        <div class="clear"></div>

    </div>
    
  <section id="footer">
    <div class="container">
      <div class="sixteen columns">
        <h2 class="message-block">
          Questions? Comments? Just wanna chat? <a href="mailto:contact@thisgoodworld.com">contact@thisgoodworld.com</a>
        </h2>
      </div> 
      <div class="sixteen columns">
        <a href="privacy-policy" id="privacy-policy">Privacy Policy</a>    
      </div>
    </div>        
       
  </section>    

</div> <!-- /#wrap --></body></html>