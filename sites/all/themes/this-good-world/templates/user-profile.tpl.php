	<?php drupal_add_css(path_to_theme() . '/css/user-profile.css', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>

	<?php drupal_add_js(path_to_theme() . '/js/user-profile.js', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>
	<?php drupal_add_js(path_to_theme() . '/js/jquery.readmore-min.js', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>
  <?php drupal_add_js(path_to_theme() . '/js/jquery.zclip.min.js', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>

  <section id="user-popup">
  	<h2><?php print render($user_profile['field_logo']['#object']->name); ?></h2>
    <?php print render ($user_profile['field_logo']); ?>
  	<?php print render($user_profile['field_address']); ?>
    <a href="<?php echo checkHttp($user_profile['field_website'][0]["#markup"]); ?>" target="_blank" id="popup-website"><?php print render ($user_profile['field_website'][0]["#markup"]); ?></a>
    <div class="area-of-good ">
    	<?php
				//determine available good
				$availableGood = array();

				if (isset($user_profile['field_for_your_community'])) {
					$availableGood[] = "community";
				}
				if (isset($user_profile['field_for_the_environment'])) {
					$availableGood[] = "environment";
				}
				if (isset($user_profile['field_for_social_change'])) {
					$availableGood[] = "social";
				}
				if (isset($user_profile['field_for_your_employees_culture'])) {
					$availableGood[] = "employee";
				}
				$randomGood = array_rand($availableGood);

				//trim if needed
				if (!function_exists('trimString')) {
					function trimString($string) {
						$string = strip_tags($string);
						$maxLength = 120;
						if (strlen($string) > $maxLength) {
								$stringCut = substr($string, 0, $maxLength);
								$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...';
						}
						return "<p>" . $string . "</p>";
					}
				}

				if (isset($randomGood)) {
					switch ($availableGood[$randomGood]) {
						case "community":
							echo '<img src="/' . path_to_theme() . '/images/community-icon.png" width="61" height="61" />
							<div class="field">';
							print trimString(render($user_profile['field_for_your_community']));
							break;
						case "environment":
							echo '<img src="/' . path_to_theme() . '/images/environment-icon.png" width="61" height="61" />
							<div class="field">';
							print trimString(render($user_profile['field_for_the_environment']));
							break;
						case "social":
							echo '<img src="/' . path_to_theme() . '/images/social-icon.png" width="61" height="61" />
							<div class="field">';
							print trimString(render($user_profile['field_for_social_change']));
							break;
						case "employee":
							echo '<img src="/' . path_to_theme() . '/images/employees-icon.png" width="61" height="61" />
							<div class="field">';
							print trimString(render($user_profile['field_for_your_employees_culture']));
							break;
					}
				}

			?>
	      <a href="javascript: void(0);" onclick="loadProfile(<?php print_r($user_profile['field_logo']['#object']->uid); ?>);" class="view-profile">View profile >></a>
      </div>
      <div class="clear"></div>
    </div>
  </section>

	<section id="user-profile">
		<?php
		global $user;
		if (arg(0) == 'user' && $user->uid == arg(1) && arg(2) == '' ){
			echo 	'
						<div class="container resource-center">
							<div class="sixteen columns">
                <h3>Resource Center</h3>
              <a href="/user/' . $user->uid . '/press-release/add?destination=user/'. $user->uid .'"><h3>Submit News Release</h3></a>
							</div>
							<div class="one-third column">
								<h4>Member Logo Download</h4>
								<a href="/sites/default/files/tgw-member-logo-web.png" download="This Good World Member Web Logo">Web</a>/
								<a href="/sites/default/files/tgw-member-logo-print.png" download="This Good World Member Print Logo">Print</a>
							</div>
							<div class="one-third column">
								<h4>Digital Membership Card</h4>
								<a href="/sites/default/files/tgw-pledge-card.png" download="This Good World Pledge Card">Download card</a>
							</div>
							<div class="one-third column">
								<h4>Blog Post</h4>
								<textarea id="blog-post-text">We have some big news.

As some of you may know, we’re committed doing doing as much good as possible. Today, we’re deepening that commitment.

We’ve just joined This Good World--a platform that connects and supports good businesses so individuals can discover and support them, too. We’ve just joined a community of like-minded businesses all over that, like us, have signed the following pledge:

“If good exists, we will find it.
When good is found, we will spread it.
If good is not yet there, we will create it.”

Beyond highlighting the good we’re doing, This Good World provides us with an opportunity to collaborate with these other amazing businesses and organizations to bring more good to the world.

We’d greatly appreciate if you visit our This Good World profile at [direct link] and throw any support you can to the other members of this good business community. Together, we can make a big difference.</textarea><a href="javascript: void(0);" id="copy-blog-text">Copy</a>
							</div>
						</div>
						<div class="container page-section">
							<div class="sixteen columns">
							<a href="/user/' . $user->uid . '/edit?destination=user/'. $user->uid .'">Edit my profile</a><br/>

							</div>
						</div>
						';
		}
		?>
		<div class="container page-section">
      <div class="six columns">
        <?php print render ($user_profile['field_logo']); ?>
        <div class="profile-description">
					<?php print render ($user_profile['field_description']); ?>
				</div>
        <h3>Connect</h3>
        <ul class="social">
          <?php if (isset($user_profile["field_facebook_url"][0]["#markup"])) { echo "<a href='mailto:" . $user_profile["field_logo"]["#object"]->mail . "' target='_blank' class='icon-envelope-alt'>"; echo "<li>Mail</li></a>"; }; ?>
          <?php if (isset($user_profile["field_website"][0]["#markup"])) { echo "<a href='" . checkHttp($user_profile["field_website"][0]["#markup"]) . "' target='_blank' class='icon-globe'>"; echo "<li>Website</li></a>"; }; ?>
          <?php if (isset($user_profile["field_foursquare_url"][0]["#markup"])) { echo "<a href='" . checkHttp($user_profile["field_foursquare_url"][0]["#markup"]) . "' target='_blank' class='icon-globe'>"; echo "<li>Foursquare</li></a>"; }; ?>
          <?php if (isset($user_profile["field_twitter_url"][0]["#markup"])) { echo "<a href='" . checkHttp($user_profile["field_twitter_url"][0]["#markup"]) . "' target='_blank' class='icon-twitter'>"; echo "<li>Twitter</li></a>"; }; ?>
          <?php if (isset($user_profile["field_facebook_url"][0]["#markup"])) { echo "<a href='" . checkHttp($user_profile["field_facebook_url"][0]["#markup"]) . "' target='_blank' class='icon-facebook'>"; print render ($user_profile['']); echo "<li>Facebook</li></a>"; }; ?>
          <?php if (isset($user_profile["field_pinterest_url"][0]["#markup"])) { echo "<a href='" . checkHttp($user_profile["field_pinterest_url"][0]["#markup"]) . "' target='_blank' class='icon-pinterest'>"; print render ($user_profile['']); echo "<li>Pinterest</li></a>"; }; ?>
          <?php if (isset($user_profile["field_instagram_url"][0]["#markup"])) { echo "<a href='" . checkHttp($user_profile["field_instagram_url"][0]["#markup"]) . "' target='_blank' class='icon-camera'>"; print render ($user_profile['["field_linkedin_url"][0]["#markup"]']); echo "<li>LinkedIn</li></a>"; }; ?>
          <?php
						if (isset($user_profile["field_linkedin_url"][0]["#markup"])) {
							echo "<a href='http://" . $user_profile["field_linkedin_url"][0]["#markup"] . "' target='_blank' class='icon-linkedin'>";
							echo "<li>LinkedIn</li></a>";
						};
					?>
          </ul>
					<div class="share-profile">
						<h3>Share this profile</h3>
				    <!-- AddThis Button BEGIN -->
    <div class="addthis_toolbox addthis_default_style addthis_32x32_style clearfix">
      <a class="addthis_button_twitter at300b">
        <img src="/sites/all/themes/this-good-world/css/images/addthis_tw.jpg"  width="32" height="32" border="0" alt="Share" />
      </a>
      <a class="addthis_button_facebook at300b">
      <img src="/sites/all/themes/this-good-world/css/images/addthis_fb.jpg"  width="32" height="32" border="0" alt="Share" />
     </a>
      <a class="addthis_button_email at300b" >
        <img src="/sites/all/themes/this-good-world/css/images/addthis_mail.jpg"  width="32" height="32" border="0" alt="Share" />
      </a>
          <a style="margin-left:10px;position:relative;top:5px;" class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
    </div>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e2e35f0b4dcba6"></script>
    <!-- AddThis Button END -->
					</div>
				</div>
      <div class="nine columns offset-by-one">
        <h2 id="profile-name"><?php print($user_profile['field_logo']['#object']->name); ?></h2>
				<?php
					$dateCreated = $user_profile['field_logo']['#object']->created;
					$foundingDate = strtotime("2014-02-01 12:00");
					if ($dateCreated < $foundingDate) {
						echo '<h6><span id="founding">Founding</span> &nbsp;&nbsp;member since ' . date('M Y', $user_profile['field_logo']['#object']->created) . '</h6>';
					}
					else {
						echo '<h6>Member since ' . date('M Y', $user_profile['field_logo']['#object']->created) . '</h6>';
					}
				?>


        <ul class="accordion" id="vertical">
        <?php
					if (isset($user_profile['field_for_your_community'])) {
						echo '  <li class="slide-01"><div class="area-of-good">
							<img src="/' . path_to_theme() . '/images/community-icon.png" width="61" height="61" />
							<h3>Community</h3>';
						print render($user_profile['field_for_your_community']);
       			echo "</div></li>";
					}
				?>

        <?php
					if (isset($user_profile['field_for_the_environment'])) {
						echo '  <li class="slide-02"><div class="area-of-good">
							<img src="/' . path_to_theme() . '/images/environment-icon.png" width="61" height="61" />
							<h3>Environment</h3>';
						print render($user_profile['field_for_the_environment']);
       			echo "</div></li>";
					}
				?>

        <?php
					if (isset($user_profile['field_for_social_change'])) {
						echo '  <li class="slide-03"><div class="area-of-good">
							<img src="/' . path_to_theme() . '/images/social-icon.png" width="61" height="61" />
							<h3>Social</h3>';
						print render($user_profile['field_for_social_change']);
       			echo "</div></li>";
					}
				?>

        <?php
					if (isset($user_profile['field_for_your_employees_culture'])) {
						echo '  <li class="slide-04"><div class="area-of-good">
							<img src="/' . path_to_theme() . '/images/employees-icon.png" width="61" height="61" />
							<h3>Employees <br />&amp; Culture</h3>';
						print render($user_profile['field_for_your_employees_culture']);
       			echo "</div></li>";
					}
				?>  </ul>

      </div>
    </div>
			<?php
				if (arg(0) == 'user' && $user->uid == arg(1) && arg(2) == '' ){
				echo '
					<div class="container page-section">
						<div class="sixteen columns">
							<a href="/user/' . $user->uid . '/edit?destination=user/'. $user->uid .'">Edit my profile</a>
						</div>
					</div>
				';
				}
			?>
  </section>
