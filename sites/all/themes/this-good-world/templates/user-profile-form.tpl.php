  <?php drupal_add_css(path_to_theme() . '/css/user-profile.css', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>

  <?php drupal_add_js(path_to_theme() . '/js/user-profile.js', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>
  <?php drupal_add_js(path_to_theme() . '/js/jquery.readmore-min.js', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>
  <?php drupal_add_js(path_to_theme() . '/js/jquery.zclip.min.js', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>

  <section id="user-profile">

    <div class="container page-section">
      <div class="six columns">
        <?php print $rendered['logo']; ?>
        <div class="profile-description">
          <?php print $rendered['field_description']; ?>
        </div>
        <h3>Connect</h3>
        <ul class="social">
          <li>
            <a class="icon-envelope-alt" href='#'></a>
            <?php print $rendered['mail']; ?>
          </li>
          <li>
            <a class="icon-globe" href='#'></a>
            <?php print $rendered['field_website']; ?>
          </li>
           <li>
            <a class="icon-globe" href='#'></a>
            <?php print $rendered['field_foursquare_url']; ?>
          </li>
           <li>
            <a class="icon-twitter" href='#'></a>
            <?php print $rendered['field_twitter_url']; ?>
          </li>
           <li>
            <a class="icon-facebook" href='#'></a>
            <?php print $rendered['field_facebook_url']; ?>
          </li>
             <li>
            <a class="icon-pinterest" href='#'></a>
            <?php print $rendered['field_pinterest_url']; ?>
          </li>
                     <li>
            <a class="icon-camera" href='#'></a>
            <?php print $rendered['field_instagram_url']; ?>
          </li>


          </ul>

        </div>
      <div class="nine columns offset-by-one">
        <h2 id="profile-name"> <?php print $username; ?></h2>
        <?php
          $dateCreated = $created;
          $foundingDate = strtotime("2014-02-01 12:00");
          if ($dateCreated < $foundingDate) {
            echo '<h6><span id="founding">Founding</span> &nbsp;&nbsp;member since ' . date('M Y', $created) . '</h6>';
          }
          else {
            echo '<h6>Member since ' . date('M Y', $created) . '</h6>';
          }
        ?>


        <ul class="accordion" id="vertical">
        <?php

            echo '  <li class="slide-01"><div class="area-of-good">
              <img src="/' . path_to_theme() . '/images/community-icon.png" width="61" height="61" />
              <h3>Community</h3>';
                  print $rendered['field_for_your_community'];
            echo "</div></li>";

        ?>

        <?php
            echo '  <li class="slide-02"><div class="area-of-good">
              <img src="/' . path_to_theme() . '/images/environment-icon.png" width="61" height="61" />
              <h3>Environment</h3>';
             print $rendered['field_for_the_environment'];
            echo "</div></li>";
        ?>

        <?php
            echo '  <li class="slide-03"><div class="area-of-good">
              <img src="/' . path_to_theme() . '/images/social-icon.png" width="61" height="61" />
              <h3>Social</h3>';
            print $rendered['field_for_social_change'];
            echo "</div></li>";
        ?>

        <?php
            echo '  <li class="slide-04"><div class="area-of-good">
              <img src="/' . path_to_theme() . '/images/employees-icon.png" width="61" height="61" />
              <h3>Employees <br />&amp; Culture</h3>';
          print $rendered['field_for_your_employees_culture'];
            echo "</div></li>";
          ?>  </ul>

      </div>
    </div>
 <div class="container page-section">
  <div class="six columns">
    <h3>General configuration</h3>
      <?php echo $rendered['field_type_of_business']; ?>
      <?php //echo $rendered['field_type_of_good']; ?>
      <?php echo $rendered['field_address']; ?>
      <?php echo $rendered['field_map_geofield']; ?>
      <?php echo $rendered['field_contact_email']; ?>
      <?php // echo $rendered['field_contact_email_2']; ?>
      <?php // echo $rendered['field_contact_email_3']; ?>
   </div>
   <div class="nine columns offset-by-one">
    <h3>Account configuration</h3>
      <?php echo $rendered['timezone']; ?>
    <?php  echo $rendered['account']; ?>




   </div>

  </div>
    <input type="hidden" name="form_id" value="<?php print $form['#form_id']; ?>" />
    <input type="hidden" name="form_build_id" value="<?php print $form['#build_id']; ?>" />
    <input type="hidden" name="form_token" value="<?php print $form['form_token']['#default_value']; ?>" />
    <?php echo $rendered['field_pledge']; ?>
    <?php echo $rendered['actions']; ?>
  </section>


