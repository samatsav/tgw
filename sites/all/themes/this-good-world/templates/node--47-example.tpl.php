	<?php 
		define('STRIPE_PRIVATE_KEY', 'sk_test_WZPVMEXku5KYeYeO8xUPZqcW');
		define('STRIPE_PUBLIC_KEY', 'pk_test_htXGTrZ8X8Ft7Hri9wCMtvEB');

		echo '<script type="text/javascript">// <![CDATA[
		Stripe.setPublishableKey("' . STRIPE_PUBLIC_KEY . '");
		// ]]></script>';

		// Check for a form submission:
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			// Stores errors:
			$errors = array();
			
			// Need a payment token:
			if (isset($_POST['stripeToken'])) {
				
				$token = $_POST['stripeToken'];
				
				// Check for a duplicate submission, just in case:
				// Uses sessions, you could use a cookie instead.
				if (isset($_SESSION['token']) && ($_SESSION['token'] == $token)) {
					$errors['token'] = 'You have apparently resubmitted the form. Please do not do that.';
				} else { // New submission.
					$_SESSION['token'] = $token;
				}		
				
			} else {
				$errors['token'] = 'The order cannot be processed. Please make sure you have JavaScript enabled and try again.';
			}
			
			// Set the order amount somehow:
			$amount = 2000; // $20, in cents
		
			// Validate other form data!
		
			// If no errors, process the order:
			if (empty($errors)) {
				
				// create the charge on Stripe's servers - this will charge the user's card
				try {
					
					// Include the Stripe library:
					require_once('includes/stripe/lib/Stripe.php');
		
					// set your secret key: remember to change this to your live secret key in production
					// see your keys here https://manage.stripe.com/account
					Stripe::setApiKey(STRIPE_PRIVATE_KEY);
		
					// Charge the order:
					$charge = Stripe_Charge::create(array(
						"amount" => $amount, // amount in cents, again
						"currency" => "usd",
						"card" => $token,
						"description" => $email
						)
					);
		
					// Check that it was paid:
					if ($charge->paid == true) {
						
						// Store the order in the database.
						// Send the email.
						// Celebrate!
						
					} else { // Charge was not paid!	
						echo '<div class="alert alert-error"><h4>Payment System Error!</h4>Your payment could NOT be processed (i.e., you have not been charged) because the payment system rejected the transaction. You can try again or use another card.</div>';
					}
					
				} catch (Stripe_CardError $e) {
						// Card was declined.
					$e_json = $e->getJsonBody();
					$err = $e_json['error'];
					$errors['stripe'] = $err['message'];
				} catch (Stripe_ApiConnectionError $e) {
						// Network problem, perhaps try again.
				} catch (Stripe_InvalidRequestError $e) {
						// You screwed up in your programming. Shouldn't happen!
				} catch (Stripe_ApiError $e) {
						// Stripe's servers are down!
				} catch (Stripe_CardError $e) {
						// Something else that's not the customer's fault.
				}
		
			} // A user form submission error occurred, handled below.
			
		} // Form submission.

		drupal_add_css(path_to_theme() . '/css/pricing.css', array('group' => CSS_THEME, 'every_page' => FALSE));
	
		drupal_add_js(path_to_theme() . '/js/register.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js('https://js.stripe.com/v2/', array('group' => CSS_THEME, 'every_page' => FALSE));

  ?>

		<div class="container">
			<div class="sixteen columns title-bar clearfix">
				<h1>Join</h1>
				<span>Businesses join here</span>
			</div>			
			<div class="sixteen columns clearfix">
				<h3>We’ve set up our membership in a way that keeps This Good World a viable option for all good businesses, even those without disposable financial resources. Please select the annual membership due that best matches your size below:</h3>
			</div>
			<div class="sixteen columns clearfix">
				<form id="payment-form" action="/join" method="POST">
					<label>Card Number</label>
					<input type="text" size="20" autocomplete="off">
					<span>Enter the number without spaces or hyphens.</span>
					<label>CVC</label>
					<input type="text" size="4" autocomplete="off">
					<label>Expiration (MM/YYYY)</label>
					<input type="text" size="2">
					<span> / </span>
					<input type="text" id="payment-submit" size="4">
					<div id="payment-errors"></div>
				</form>
			</div>
			<div class="four columns">
				<a href="#" class="pricing-option">
					<img src="/<?php echo path_to_theme(); ?>/images/large-business-icon.png" alt="Large company icon" class="company-icon" />
					<div class="price">$2,500/yr</div>
					<hr/>
					<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
				</a>
			</div>
			<div class="four columns">
				<a href="#" class="pricing-option">
					<img src="/<?php echo path_to_theme(); ?>/images/medium-business-icon.png" alt="Medium company icon" class="company-icon" />
					<div class="price">$1,000/yr</div>
					<hr/>
					<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
				</a>
			</div>
			<div class="four columns">
				<a href="#" class="pricing-option">
					<img src="/<?php echo path_to_theme(); ?>/images/small-business-icon.png" alt="Small company icon" class="company-icon" />
					<div class="price">$250/yr</div>
					<hr/>
					<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
				</a>
			</div>
			<div class="four columns">
				<a href="#" class="pricing-option">
					<img src="/<?php echo path_to_theme(); ?>/images/free-business-icon.png" alt="Free company icon" class="company-icon" />
					<div class="price">$0/yr</div>
					<hr/>
					<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
				</a>
			</div>
			<div class="sixteen columns clearfix">
				<h3>The Proportional Benefit</h3>
				<p>Financially (and hypothetically), if becoming a This Good World member results in a 1% increase in business, that 1% is different for every member. To keep things as equal as possible, we think it's fair that annual membership dues should align with this same proportional thinking.</p>
				<hr class="orange" />
				<p class="call-out">Have multiple locations or  a franchise? <a href="#">Contact us</a> for a custom membership package.</p>
			</div>
		</div>
		