	<!--Pricing Page-->

<?php

		drupal_add_css(path_to_theme() . '/css/pricing.css', array('group' => CSS_THEME, 'every_page' => FALSE));

		drupal_add_js('https://checkout.stripe.com/checkout.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		//drupal_add_js('https://js.stripe.com/v2/', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/join-half.js', array('group' => CSS_THEME, 'every_page' => FALSE));

  ?>

		<div class="container">
			<div class="sixteen columns title-bar clearfix">
				<h1>Let’s go Halfsies</h1>
				<span>Half the price, just as nice</span>
			</div>

<div class="eight columns"><h2 class="firststep"><strong>1.</strong> Select the plan that is best for your business</h2></div>
<div class="eight offset-by-one columns"><h2 class="secondstep"><strong>2.</strong> Fill out the short registration form</h2></div>
<div class="sixteen columns"><div class="thumbs  ">That's it</div></div>

			<div class="sixteen columns clearfix">
				<form action="/register" method="POST" id="checkout">
				</form>
				<p>Looks like you discovered our half off land! We'd like to invite you to join This Good World at half the price for life. Please select the annual membership level below that best matches your size.</p>



			</div>
			<div class="four columns">
<h3>Small Business <span>1-5 people</span></h3>
<a id="halfsmall" class="pricing-option">
<img src="/<?php echo path_to_theme(); ?>/images/small-business-icon.png" alt="Small company icon" class="company-icon" />
<div class="price">$125/yr</div>
<hr/>
<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
</a>

<a id="halfsmallmonth" class="pricing-option">
<i>pay by month</i>
</a>


</div>
			<div class="four columns">
				<h3>Medium <span>6-20</span></h3>
				<a id="halfmedium" class="pricing-option">
					<img src="/<?php echo path_to_theme(); ?>/images/medium-business-icon.png" alt="Medium company icon" class="company-icon" />
					<div class="price">$500/yr</div>
					<hr/>
					<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
				</a>

                <a id="halfmediummonth" class="pricing-option">
<i>pay by month</i>
</a>
			</div>
			<div class="four columns">
                <h3>Large <span>20+ people</span></h3>
				<a id="halflarge" class="pricing-option">
					<img src="/<?php echo path_to_theme(); ?>/images/large-business-icon.png" alt="Large company icon" class="company-icon" />
					<div class="price">$1,250/yr</div>
					<hr/>
					<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
				</a>
                <a id="halflargemonth" class="pricing-option">
<i>pay by month</i>
</a>
			</div>
			<div class="four columns">
				<h3>Small Nonprofits <span>+ limited resources</span></h3>
				<a href=memberrequest class="pricing-option">
					<img src="/<?php echo path_to_theme(); ?>/images/free-business-icon.png" alt="Free company icon" class="company-icon" />
					<div class="price">$0/yr</div>
					<hr/>
					<img src="/<?php echo path_to_theme(); ?>/images/sign-up-button.png" alt="Sign up" class="sign-up-button" />
				</a>
			</div>
			<div class="sixteen columns clearfix">
				<h3>The Proportional Benefit</h3>
				<p>Financially (and hypothetically), if becoming a This Good World member results in a 1% increase in business, that 1% is different for every member. To keep things as equal as possible, we think it's fair that annual membership dues should align with this same proportional thinking.</p>
				<hr class="orange" />
				<p class="call-out">Have multiple locations or  a franchise? <a href="#">Contact us</a> for a custom membership package.</p>
			</div>
		</div>

