	<?php drupal_add_css(path_to_theme() . '/css/blogs.css', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>

	<div class="container">
<div class="six columns ">
	<span class="press-date"><?php print $date; ?></span>
	<h1><?php print $title; ?></h1>
		<?php print render($content['field_image']); ?>
</div>
<div class="ten columns">
		<h2><?php print render($content['field_subtitle']); ?></h2>
			<!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style addthis_32x32_style clearfix">
			<a class="addthis_button_twitter at300b">
				<img src="/sites/all/themes/this-good-world/css/images/addthis_tw.jpg"  width="32" height="32" border="0" alt="Share" />
			</a>
			<a class="addthis_button_facebook at300b">
				<img src="/sites/all/themes/this-good-world/css/images/addthis_fb.jpg"  width="32" height="32" border="0" alt="Share" />
			</a>
			<a class="addthis_button_email at300b" >
				<img src="/sites/all/themes/this-good-world/css/images/addthis_mail.jpg"  width="32" height="32" border="0" alt="Share" />
			</a>
			<a style="margin-left:10px;position:relative;top:5px;" class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e2e35f0b4dcba6"></script>
		<!-- AddThis Button END -->
		<?php print render($content['body']); ?>
			<?php print render($content['field_video']); ?>
			<?php print render($content['field_category']); ?>
			<?php print render($content['field_tags']); ?>
			<h2><a href="/news-room" class="back">Back to News Room</a></h2>
</div>


</div>






