<?php
     drupal_add_css(path_to_theme() . '/css/members.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE));

    drupal_add_js(path_to_theme() . '/js/jquery-ui-1.8.7.custom.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
    drupal_add_js(path_to_theme() . '/js/modernizr.custom.23663.js', array('group' => CSS_THEME, 'every_page' => FALSE));
    drupal_add_js(path_to_theme() . '/js/members.js', array('group' => CSS_THEME, 'every_page' => FALSE));
?>

    <div class="container">

        <?php if ($page['sidebar_first']) { ?>

        <div id="content" class="eleven columns">

        <?php } else { ?>

        <div id="content" class="page-wrapper">

        <?php } ?>

            <?php if ($messages): ?>

                <div id="messages">

                  <?php print $messages; ?>

                </div><!-- /#messages -->

            <?php endif; ?>


                <?php if ($tabs): ?>

                <div class="tabs">

                  <?php print render($tabs); ?>

                </div>

                <?php endif; ?>



                <?php print render($page['help']); ?>

        <div class="sixteen  columns title-bar">
            <h1>MEMBERS</h1>
            <span>Discover Businesses Doing Good Things</span>
        </div>
        <h2 style="text-align:center; border-bottom:none; font-wight:bold;">Browse by CATEGORY</h2>

        <?php echo views_embed_view( 'Members', $display_id='block_1' ); ?>
        <div class="clearfix"></div>
    <h2 style="text-align:center; border-bottom:none;margin-top:30px; border-top:1px solid #e7e7e7;font-wight:bold;">Browse by CITY</h2>


                <?php print render($page['content']); ?>


        </div><!-- /#content -->


        <div class="clear"></div>

    </div>

