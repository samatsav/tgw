(function ($) {
	$(document).ready(function () {
		
		mainNav = '' + 
			'<nav class="menu-holder">' + 
				'<ul id="main-menu" class="menu-list">' + 
					'<li>' + 
						'<a href="/category/offers">Offers' + 
							'<span class="arrow"></span>' +
						'</a>' + 
					'</li>' +
					'<li>' +
						'<a href="/category/needs">Needs' +
							'<span class="arrow"></span>' +
						'</a>' +
					'</li>' +
				'</ul>' +
			'</nav>';
		
		$("header .title").append(mainNav);
		
	});
})(jQuery);