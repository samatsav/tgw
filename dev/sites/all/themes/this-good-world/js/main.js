(function ($) {
	$(document).ready(function () {
		
		$('#subMenu').change(function() {
			var who = $(this).val();
			if (who.indexOf("\/") > -1) {
				window.location = who;
			}
			else {;
				var goPosition = $(who).offset().top - 73;
				$('html,body').animate({ scrollTop: goPosition}, 'slow');
			}
		});
		
		$(".close, .bubble").click(function() {
			$(".message-box").hide("450");
		});
		
	});
})(jQuery);