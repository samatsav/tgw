$(document).ready(function() {
	$("#payment-form").submit(function(event) {
			$('#payment-submit').attr('disabled', 'disabled');
			return false;
		
			var error = false;
			 
			// Get the values:
			var ccNum = $('.card-number').val(),
					cvcNum = $('.card-cvc').val(),
					expMonth = $('.card-expiry-month').val(),
					expYear = $('.card-expiry-year').val();
			 
			// Validate the number:
			if (!Stripe.validateCardNumber(ccNum)) {
					error = true;
					reportError('The credit card number appears to be invalid.');
			}
			 
			// Validate the CVC:
			if (!Stripe.validateCVC(cvcNum)) {
					error = true;
					reportError('The CVC number appears to be invalid.');
			}
			 
			// Validate the expiration:
			if (!Stripe.validateExpiry(expMonth, expYear)) {
					error = true;
					reportError('The expiration date appears to be invalid.');
			}		
		
			if (!error) {
					// Get the Stripe token:
					Stripe.createToken({
							number: ccNum,
							cvc: cvcNum,
							exp_month: expMonth,
							exp_year: expYear
					}, stripeResponseHandler);
			 }
		
			function stripeResponseHandler(status, response) {
				if (response.error) {
					reportError(response.error.message);
				} 
				else {
					// Get a reference to the form:
					var f = $("#payment-form");
					 
					// Get the token from the response:
					var token = response.id;
					 
					// Add the token to the form:
					f.append('<input type="hidden" name="stripeToken" value="' + token + '" />');
					 
					// Submit the form:
					f.get(0).submit();
				}				
			}	
		
			function reportError(msg) {
				// Show the error in the form:
				$('#payment-errors').text(msg).addClass('error');
		 
				// Re-enable the submit button:
				$('#submitBtn').prop('disabled', false);
		 
				return false;
			}		
		
	});
});