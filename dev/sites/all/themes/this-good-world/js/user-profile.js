(function ($) {
	$(document).ready(function () {
		$('a#copy-blog-text').zclip({
			path:'http://www.steamdev.com/zclip/js/ZeroClipboard.swf',
			copy:$('textarea#blog-post-text').text()
		});
		$('.profile-description .field-item, .area-of-good .field-item').readmore({
			speed: 600,
			maxHeight: 165,
			moreLink: '<a href="#">Read more</a>',
			lessLink: '<a href="#">Collapse</a>'
		});			
	});
})(jQuery);