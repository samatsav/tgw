(function ($) {
	$(document).ready(function () {
		var large_handler = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(250000);
				var $plan_id = $('<input type=hidden name=planId />').val("largeyear");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('large-business').addEventListener('click', function(e) {
			// Open Checkout with further options
			large_handler.open({
				name: 'This Good World',
				description: 'Please enter payment information',
				amount: 250000
			});
			e.preventDefault();
		});
		
		var medium_handler = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(100000);
				var $plan_id = $('<input type=hidden name=planId />').val("mediumyear");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('medium-business').addEventListener('click', function(e) {
			// Open Checkout with further options
			medium_handler.open({
				name: 'This Good World',
				description: 'Please enter payment information',
				amount: 100000
			});
			e.preventDefault();
		});
		
		var small_handler = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(25000);
				var $plan_id = $('<input type=hidden name=planId />').val("smallyear");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('small-business').addEventListener('click', function(e) {
			// Open Checkout with further options
			small_handler.open({
				name: 'This Good World',
				description: 'Please enter payment information',
				amount: 25000
			});
			e.preventDefault();
		});

		/*
		var small_handler = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(25000);

      	$('#checkout').append($input, $amount).submit();
			}
		});
	
		document.getElementById('small-business').addEventListener('click', function(e) {
			// Open Checkout with further options
			small_handler.open({
				name: 'This Good World',
				description: 'Small Business Subscription',
				amount: 25000
			});
			e.preventDefault();
		});
		*/

		var small_handler_monthly = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(2500);
				var $plan_id = $('<input type=hidden name=planId />').val("smallmonth");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('smallmonth').addEventListener('click', function(e) {
			// Open Checkout with further options
			small_handler_monthly.open({
				name: 'This Good World',
				description: 'Small Business Monthly',
				currency: "usd",
				interval: "month",
				trial_period_days: "0",
				amount: 2500
			});
			e.preventDefault();
		});
		
		var medium_handler_monthly = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(9500);
				var $plan_id = $('<input type=hidden name=planId />').val("mediummonth");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('mediummonth').addEventListener('click', function(e) {
			// Open Checkout with further options
			medium_handler_monthly.open({
				name: 'This Good World',
				description: 'Medium Business Monthly',
				currency: "usd",
				interval: "month",
				trial_period_days: "0",
				amount: 9500
			});
			e.preventDefault();
		});
		
		var large_handler_monthly = StripeCheckout.configure({
			key: 'pk_live_YGHwEs0cFD0tZ1Cap1MEegH9',
			image: 'https://thisgoodworld.com/sites/default/files/stripe-sign.png',
			token: function(token, args) {
				var $input = $('<input type=hidden name=stripeToken />').val(token.id);
				var $amount = $('<input type=hidden name=stripeAmount />').val(22500);
				var $plan_id = $('<input type=hidden name=planId />').val("largemonth");
      			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
      			$('#checkout').append($input, $amount, $plan_id, $email).submit();
			}
		});
	
		document.getElementById('largemonth').addEventListener('click', function(e) {
			// Open Checkout with further options
			large_handler_monthly.open({
				name: 'This Good World',
				description: 'Large Business Monthly',
				currency: "usd",
				interval: "month",
				trial_period_days: "0",
				amount: 22500
			});
			e.preventDefault();
		});
		
	});
})(jQuery);