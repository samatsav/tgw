(function ($) {
	$(document).ready(function () {
		$(".question").click(function() {
			$(this).parent().find(".answer").toggle("blind", {direction: "vertical"}, "slow");
		});
	});
})(jQuery);		