<div class="container">

		<!-- SECTION TITLE -->

		<div class="sixteen columns title-bar">
			<h1>Get Involved</h1><span>Help us spread the good</span>
		</div>

	</div>
	<!-- ./container -->

	<div class="container">

		<div class="sixteen columns">
			<p>We're so glad you're here. And we sure would appreciate your help spreading the good.</p>
				<ul class="large-list">
					<li>First and foremost, check out our <a href="/">members</a> and <em>support them</em>.</li>
					<li>Follow us on Twitter and Facebook:
						<ul class="contact-list">
							<a href="http://www.facebook.com/thisgoodworld" target="_blank">
								<li><i class="icon-facebook"></i>Facebook</li>
							</a>
							<a href="http://www.twitter.com/thisgoodworld" target="_blank">
								<li><i class="icon-twitter"></i>Twitter</li>
							</a>
						</ul>
					</li>
					<li style="display: none;">Support us:
						<ul class="contact-list">
							<li><a id="three" class="support-option">$3/month</a></li>
							<li><a id="seven" class="support-option">$7/month</a></li>
							<li><a id="nine" class="support-option">$9/month</a></li>
						</ul>
						<em>Your contributions help us expand This Good World to new cities and towns all over. This financial support is a huge part of what allows us to take care of some very important things that help This Good World keep truckin’ (from server space to food for our pups).</em>
					</li>
					<li>Use <em>#thisgoodworld</em> and help spread the good.</li>
					<li>Know a business that would dig This Good World? Tell them: 
							<textarea style="width: 300px;">Hi there [insert cool biz], 
	
	I just stumbled onto This Good World and thought you'd be a great fit for their network of good businesses!
	
	It would be awesome to have more dedicated organizations in our town be more easily discovered, supported and ultimately come together.
	
	Check out thisgoodworld.com and help spread the good!
							</textarea> </li>
					<li>Interested in becoming a city ambassador? <a href="/contact">Contact us</a> today.</li>
					<li>Interested in being featured in our photo challenge, “7 days of Good” on Instagram? <a href="/contact">Shoot us a note</a>.</li>
					<li>Or <a href="/contact">let us know</a></li>
				</ul>
        
		</div>

	</div>
	<!-- ./container -->

    <div class="container sixteen page-section">
        <a href="https://thisgoodworld.com/join" class="join-button">
            <img src="<?php echo path_to_theme(); ?>/images/businesses-join.png" alt="Business join here" />
        </a>	
    </div>