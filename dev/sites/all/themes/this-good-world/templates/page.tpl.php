

    <div class="container">

        <?php if ($page['sidebar_first']) { ?>

        <div id="content" class="eleven columns">

        <?php } else { ?>

        <div id="content" class="page-wrapper">

        <?php } ?>

      
            <?php if ($messages): ?>

                <div id="messages">

                  <?php print $messages; ?>

                </div><!-- /#messages -->

            <?php endif; ?>

                <?php print render($title_prefix); ?>

                

                <?php if ($title): ?>

                <h1 class="title" id="page-title">

                  <?php print $title; ?>

                </h1>

                <?php endif; ?>

                

                <?php print render($title_suffix); ?>

                

                <?php if ($tabs): ?>

                <div class="tabs">

                  <?php print render($tabs); ?>

                </div>

                <?php endif; ?>

                

                <?php print render($page['help']); ?>


                <?php print render($page['content']); ?>


        </div><!-- /#content -->


        <div class="clear"></div>

    </div>
    
