<?php /** * @file * Default theme implementation to display the basic html structure of a single * Drupal page. * * Variables: * - $css: An array of CSS files for the current page. * - $language: (object) The language the site is being displayed in. * $language->language contains its textual representation. * $language->dir contains the language direction. It will either be 'ltr' or 'rtl'. * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document. * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data. * - $head_title: A modified version of the page title, for use in the TITLE * tag. * - $head_title_array: (array) An associative array containing the string parts * that were used to generate the $head_title variable, already prepared to be * output as TITLE tag. The key/value pairs may contain one or more of the * following, depending on conditions: * - title: The title of the current page, if any. * - name: The name of the site. * - slogan: The slogan of the site, if any, and if there is no title. * - $head: Markup for the HEAD section (including meta tags, keyword tags, and * so on). * - $styles: Style tags necessary to import all CSS files for the page. * - $scripts: Script tags necessary to load the JavaScript files and settings * for the page. * - $page_top: Initial markup from any modules that have altered the * page. This variable should always be output first, before all other dynamic * content. * - $page: The rendered page content. * - $page_bottom: Final closing markup from any modules that have altered the * page. This variable should always be output last, after all other dynamic * content. * - $classes String of classes that can be used to style contextually through * CSS. * * @see template_preprocess() * @see template_preprocess_html() * @see template_process() * * @ingroup themeable */ ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"

  "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" version="XHTML+RDFa 1.0" dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>>



<head profile="<?php print $grddl_profile; ?>">

	<?php print $head; ?>

	<title>

		<?php print $head_title; ?>

	</title>

	<?php print $styles; ?>

	<?php print $scripts; ?>



	<script type="text/javascript">

		//Google Anayltics

		var _gaq = _gaq || [];

		_gaq.push(['_setAccount', 'UA-12428569-2']);

		_gaq.push(['_trackPageview']);



		(function() {

			var ga = document.createElement('script');

			ga.type = 'text/javascript';

			ga.async = true;

			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

			var s = document.getElementsByTagName('script')[0];

			s.parentNode.insertBefore(ga, s);

		})();

	</script>


<meta name="google-site-verification" content="1SXocZmd4F5ZC7s3C_VeTL1cfiZjfb3eFyTCySOUmk0" />


</head>



<body class="<?php print $classes; ?>" <?php print $attributes;?>>

	<section class="menu is-sticky">

		<div class="container">



			<div class="sixteen columns clearfix">



				<div class="logo">

					<a href="/">

						<img src="<?php echo "/" . path_to_theme(); ?>/images/logo-footer.png" width="171" height="23" alt="This Good World">

					</a>

				</div>







				<nav class="menu-holder">

					<ul id="main-menu" class="menu-list">

						<li>

							<a href="/#the-goods">The Goods

								<span class="arrow"></span>

							</a>

						</li>

						<li>

							<a href="/members">Members

								<span class="arrow"></span>

							</a>

						</li>

						<li>

							<a href="/social">Social

								<span class="arrow"></span>

							</a>

						</li>

						<li>

							<a href="/about">About

								<span class="arrow"></span>

							</a>

						</li>

						<li>

							<a href="/businesses">For Businesses

								<span class="arrow"></span>

							</a>

						</li>

						<li>

							<a href="/contact">Contact

								<span class="arrow"></span>

							</a>

						</li>

						<li>

							<a href="https://thisgoodworld.com/join" id="join" class="join-button">

								<img src="<?php echo "/" . path_to_theme(); ?>/images/businesses-join.png" width="85" height="75" />

							</a>

						</li>

					</ul>



					<!-- SubMenu -->



					<select size="1" id="subMenu" class="sub-menu" name="subMenu" onchange="moveTo(this.value)">

						<option selected="selected" value="">Menu</option>

						<option value="/">-Map</option>

						<option value="/#the-goods">-The Goods</option>

						<option value="/#social”>-Social</option>

						<option value="/about-us">- About Us</option>

						<option value="/businesses">- Businesses</option>

						<option value="/contact">- Contact</option>

						<option value="/join">- Join Us</option>

					</select>

				</nav>

				<div class="login-subscribe clearfix">
					<a href="/user/login"  class="login-button">Member Login</a>
					<a href="#"   class="subscribe-button">Subscribe</a>

				</div>
				<div class="subscribe-form">
					<span class="subscribe-close">x</span>
					<span class="subscribe-placeholder">keep in touch!<span>your@email.com</span></span>
						<?php
						$block = module_invoke('webform', 'block_view', 'client-block-45');
						// print render($block['content']);
						?>
						<!-- Begin MailChimp Signup Form -->
						<div id="mc_embed_signup">
						<form action="http://thisgoodworld.us5.list-manage1.com/subscribe/post?u=dddf81fb84a08c3a2d5fdfa7b&amp;id=97f354a4d0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

						<div class="mc-field-group" style="float:left">
							<label for="mce-EMAIL">Email Address </label>
							<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
						</div>
							<div id="mce-responses" class="">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;"><input type="text" name="b_dddf81fb84a08c3a2d5fdfa7b_97f354a4d0" tabindex="-1" value=""></div>
						    <div class="form-actions" style="float:left;"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="form-submit"></div>
						</form>
						</div>

						<!--End mc_embed_signup-->


<a href=“/contact”  class="login-button">Contact</a>
					<a href="contact"   class="contact-button">Contact</a><img src="<?php echo "/" . path_to_theme(); ?>/images/contact_thisgoodworld.png"

				</div>

				</div>




			</div>

			<!-- ./sixteen columns -->





		</div>

		<!-- ./container -->

	</section>

	<!-- ./menu -->



	<?php print $page_top; ?>

	<?php print $page; ?>

	<?php print $page_bottom; ?>



	<section class="contact page-section">



		<div class="container panel-wrap">

			<div class="one-third column">

				<h3>Who We Are</h3>

				<p>A search + discovery platform that highlights and supports businesses doing <em>good</em> things.</p>

                <h3><a href="http://thisgoodworld.com/members">Member List</a></h3>

			</div>

			<div class="one-third column">

				<h3>Get In Touch</h3>

				<ul class="list" id="contact">

					<a href="mailto:contact@thisgoodworld.com" target="_blank">

						<li class="first">

							<i class="icon-envelope-alt"></i>Email</li>

					</a>

					<a href="http://www.facebook.com/thisgoodworld" target="_blank">

						<li>

							<i class="icon-facebook"></i>Facebook</li>

					</a>

					<a href="http://www.twitter.com/thisgoodworld" target="_blank">

						<li>

							<i class="icon-twitter"></i>Twitter</li>

					</a>

					<a href="http://www.instagram.com/thisgoodworld" target="_blank">

						<li class="last">

							<i class="icon-camera"></i>Instagram</li>

					</a>

				</ul>

			</div>



			<div class="one-third column">

				<h3>News, updates and all things good</h3>

				<?php

					$block = module_invoke('webform', 'block_view', 'client-block-45');

					//print render($block['content']);

				?>
						<!-- Begin MailChimp Signup Form -->
						<div id="mc_embed_signup">
						<form action="http://thisgoodworld.us5.list-manage1.com/subscribe/post?u=dddf81fb84a08c3a2d5fdfa7b&amp;id=97f354a4d0" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate webform-client-form" target="_blank" novalidate>

						<div class="mc-field-group" style="float:left">

							<input type="email" value="" name="EMAIL" class="required email" placeholder="Enter your email" id="mce-EMAIL">
						</div>
							<div id="mce-responses" class="">
								<div class="response" id="mce-error-response" style="display:none"></div>
								<div class="response" id="mce-success-response" style="display:none"></div>
							</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
						    <div style="position: absolute; left: -5000px;"><input type="text" name="b_dddf81fb84a08c3a2d5fdfa7b_97f354a4d0" tabindex="-1" value=""></div>
						    <div class="form-actions" style="float:left;"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="form-submit"></div>
						</form>
						</div>

						<!--End mc_embed_signup-->

			</div>

		</div>

	</section>



	<section id="footer">

		<div class="container">

			<div class="sixteen columns">



				<h3 class="email">

					Questions? Interested in sponsoring a city? Just wanna chat?

					<a href="/send-message">Send us a message</a>

				</h3>

			</div>

			<div class="sixteen columns">

				<div id="copyright">&copy; 2014. All rights reserved.</div>

				<a href="/privacy-policy" id="privacy-policy">Terms &amp; Privacy Policy</a>

			</div>

		</div>

	</section>
<script type="text/javascript">
jQuery(document).ready(function () {
	jQuery('.subscribe-form .form-actions .form-submit').val('SIGN UP');
	jQuery('.subscribe-form input[type="email"]').val('');
	jQuery('.subscribe-form .subscribe-placeholder').click(function(){
		jQuery(this).hide();
		jQuery('.subscribe-form input[type="email"]').focus();
	});
	jQuery('.subscribe-button').click(function(e){
		e.preventDefault();
		jQuery('.login-subscribe').hide();
		jQuery('.subscribe-form').fadeIn();
	});
	jQuery('.subscribe-close').click(function(){
		jQuery('.subscribe-form').hide();
		jQuery('.login-subscribe').fadeIn();

	});
});
</script>
</body>



</html>

