<?php 
		drupal_add_js(path_to_theme() . '/js/businesses.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/jquery-ui-1.8.7.custom.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
  ?>


	<div class="container">


		<!-- SECTION TITLE -->

		<div class="sixteen columns title-bar">
			<h1>Businesses</h1>
			<span>Why join This Good World?</span>
		</div>

	</div>
	<!-- ./container -->

	<div class="container">

		<div class="sixteen columns">
			<h2>Share your good</h2>
			<p>Are you a socially conscious business who strives to have a positive impact on your local community, the environment, your employees or society overall? If yes, there are a lot of people that want to support you because of it.</p>
<p>Enter This Good World: A search-and-discovery tool that allows people to find and support businesses doing good things, whether they’re local or traveling from out of town. A great way to get new foot or site traffic. It’s also a site for businesses to connect, collaborate, and make action happen within their local city and beyond.</p>
<p>Organizations of all shapes, sizes, industries and types can become members of This Good World. The platform is designed to give all an equal voice—from mom & pops to big box shops. All members receive equal benefits, because all are committed to one equal thing—finding, spreading and creating more good.</p>

<div class="container sixteen">
				<img src="<?php echo path_to_theme(); ?>/images/goodicons1.png" alt=“types of good” />
		
		</div>

<div class="container sixteen">
			<a href="https://thisgoodworld.com/join" class="join-button">
				<img src="<?php echo path_to_theme(); ?>/images/businesses-join.png" alt="Business join here" />
			</a>	
		</div>

						<h2>How it works</h2>
<p>
<h3>1. Search + Discovery</h3>
<em>Our search + discovery tools provide a simple way for consumers to discover the good you do – they can see that you care about what they care about and they'll come knocking before you know it.</br></em>
<br>
<div class="container sixteen">
				<img src="<?php echo path_to_theme(); ?>/images/tgw-laptop-2.png" alt=“come together” />
		
		</div>

<p><h3>2. Collaboration</h3>
<em>This Good World is a place for learning, sharing ideas, and bringing forth topics that are meaningful to you and your business so you can work with other members to get more good done.</em></p>
<p>By joining, your business will gain exposure to a large group of individuals ready and wanting to support conscious businesses with their wallets at a lower cost and a more targeted approach than other traditional marketing methods (view our yearly membership <a href=/join>pricing</a>).</p>






			<a name="memberbenefits">


<div class="container sixteen">
				<img src="<?php echo path_to_theme(); ?>/images/tgw-cometogether.png" alt=“come together” />
		
		</div>




		</div>

</div>

	</div>
</a>
	<!-- ./container -->

		

	

<div class="container">


		<!-- SECTION TITLE -->

		<div class="sixteen columns title-bar">
			<h1>Member Benefits</h1><span>
		</div>

	</div>
	<!-- ./container -->


      <div class="description">
          TGW offers several membership benefits to meet the needs of your organization.
<br><em>All members, regardless of size or membership level, receive the same benefits and opportunities from This Good World. Member benefits include:</em></br>
        
<div class="container">
		<!-- Exposure -->

		<div class="sixteen columns title-bar">
			<h2>Exposure</h2></div>
</div>
	<!-- ./container -->        

<div class="call-out"><span>• </span>A Spot on the Map</div>
Membership includes addition to This Good World map to be easily discovered by like-minded consumers and other businesses
</br><br>
<div class="call-out"><span>• </span>A Profile</div>
Editable profile to tell the world about the good you do <a href=http://thisgoodworld.com/theskinnypancake>(example)</a href>
</br><br>
<div class="call-out"><span>• </span>Social promotion</div>
Promotion of all member web properties and social channels
</br><br>
<div class="call-out"><span>• </span>TGW Member swag</div>
Digital member logo for all and window decal for brick & mortars
</br><br>
<div class="call-out"><span>• </span>Logo visibility on thisgoodworld.com</div>
    </section>   
<p>
<div class="container">
		<!-- Collaboration -->

		<div class="sixteen columns title-bar">
			<h2>Collaboration</h2>
		</div>

	</div>
	<!-- ./container -->    
<div class="call-out"><span>• </span>Member collaboration opportunities facilitated by our team</div>
Opportunity to connect with like-minded businesses to come together to do more good
</br><br>
<div class="call-out"><span>• </span>Collaboration Tools</div>
Access to Easy-to-use Collaboration and Member Community tools<em> (coming soon)</em>
</p>

         </section>
<p>
<div class="container">
		<!-- Communication Support -->

		<div class="sixteen columns title-bar">
			<h2>Communication Support</h2>
		</div>

	</div>
	<!-- ./container -->  

<div class="call-out"><span>• </span>Member News Room</div>
Unlimited uploads to the Member News Room (portal for member press releases, article/feature links, etc.)<em> (coming soon)</em>
</br><br>
<div class="call-out"><span>• </span>Resource Center</div>
Access to member resource center 
</br><br>
<div class="call-out"><span>• </span>Monthly Member Newsletter</div>
</p>
         </section>
<p>
<div class="container">
		<!-- Other Benefits -->

		<div class="sixteen columns title-bar">
			<h2>Other Benefits</h2>
		</div>

	</div>
	<!-- ./container -->  
<div class="call-out"><span>• </span>Discounts</strong></div>
Access to featured member discount opportunities</div>
</br><br>
<div class="call-out"><span>• </span>Events</div>
Access to member events and networking opportunities in your area <em>(coming soon)</em>
</p>
         </section>



	<!-- ./container -->
<div class="container sixteen">
			<a href="https://thisgoodworld.com/join" class="join-button">
				<img src="<?php echo path_to_theme(); ?>/images/businesses-join.png" alt="Business join here" />
			</a>	
		</div>





	
	<div class="container page-section">

		<div class="sixteen columns title-bar">
			<h2>FAQ</h2>
		</div>

			<div class="container">

				<div class="eight columns">
					<div class="item">
						<a href="javascript: void(0);" class="question">
							<i class="icon-arrow-down"></i>
							<h3>How do you judge can join?</h3>
						</a>
						<div class="answer">
							<p>This Good World is not a certification organization. We choose to focus on the actual good things businesses do and the commitment to their continuing impact rather than simply labeling a business "good enough" or not.<br>

We believe good comes in all shapes and sizes, all of which is important. Whether a business is active with environmental issues, promoting social change, bringing positive action to the community, doing great things for their employees or all of the above, This Good World lets people know about it.</br>

<br>Our process includes a member pledge commitment and a simple vetting process.</br>
</p>
						</div>
					</div>

					<div class="item">
						<a href="javascript: void(0);" class="question">
							<i class="icon-arrow-down"></i>
							<h3>I'm a business interested in This Good World. How do I join?</h3>
						</a>
						<div class="answer">
							<p>Signing up for membership to This Good World is easy. Go to
								<a href="https://thisgoodworld.com/join">https://thisgoodworld.com/join</a> and fill out the form there. It should take about 5-10 minutes. Once submitted for approval and your first membership due is accepted, our team will review your information and get to work on your profile and spot on the map. This process will be quick and we'll send you a note, along with a package of goodies as soon as it's done.</p>
						</div>
					</div>

					<div class="item" style="display: none;">
						<a href="javascript: void(0);" class="question">
							<i class="icon-arrow-down"></i>
							<h3>I'm an individual, do I have to join or sign up for anything?</h3>
						</a>
						<div class="answer">
							<p>Nope. Individuals get free access to This Good World to discover good businesses whenever they please. Sit back, navigate the map, find the good and go help spread it.</p>
						</div>
					</div>

					<div class="item">
						<a href="javascript: void(0);" class="question">
							<i class="icon-arrow-down"></i>
							<h3>But, my company didn't single-handedly save the world last year. Is this still for us?</h3>
						</a>
						<div class="answer">
							<p>We strongly believe that good of all shapes and sizes is extremely important. Whether your good makes one person smile or elicits sweeping social change, we want to help spread it.</p>
						</div>
					</div>
				</div>

				<div class="eight columns">
					<div class="item">
						<a href="javascript: void(0);" class="question">
							<i class="icon-arrow-down"></i>
							<h3>What do you do with our credit card information?</h3>
						</a>
						<div class="answer">
							<p>Directly? Nothing! We process all membership dues/payments through an extremely safe, super trusted 3rd party (
								<a href="https://stripe.com" target="_blank">https://stripe.com</a>). We don't store your card number, or even see it for that matter. We leave the financial nuts and bolts up to the experts to ensure safety and privacy for all members.
								<p>
						</div>
					</div>

					<div class="item" style="display: none;">
						<a href="javascript: void(0);" class="question">
							<i class="icon-arrow-down"></i>
							<h3>What's with these recurring payments?</h3>
						</a>
						<div class="answer">
							<p>This Good World membership payments are set up on a recurring annual basis. Our 3rd party payment partner processes these payments automatically. Our goal is to be open and transparent about membership payments, so please send us a note at
								<a href="mailto:thisgoodworld.com">contact@thisgoodworld.com</a> if you have any questions or concerns.</p>
						</div>
					</div>

					<div class="item">
						<a href="javascript: void(0);" class="question">
							<i class="icon-arrow-down"></i>
							<h3>Can we cancel our membership?</h3>
						</a>
						<div class="answer">
							<p>Sure. You can cancel at any time. Just <a href="/send-message">send us a note</a> and we'll take care of it for you.</p>
						</div>
					</div>
					
				</div>
			</div>
		</div>