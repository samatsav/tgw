	<?php drupal_add_css(path_to_theme() . '/css/blogs.css', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>
	
	<div class="container">
		<?php print render($content['field_header_image']); ?>
		<?php print render($content['field_header_video']); ?>
		<!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style addthis_32x32_style clearfix">
			<a class="addthis_button_twitter"></a>
			<a class="addthis_button_facebook"></a>
			<a class="addthis_button_facebook_like" fb:like:layout="box_count"></a>
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e2e35f0b4dcba6"></script>
		<!-- AddThis Button END -->
    <div class="center-column">
			<?php print render($content['body']); ?>
      <h2><a href="/goodspotlight" class="back">Read more Good Spotlights</a></h2>
    </div>
	</div>

</div>














