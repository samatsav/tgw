<div class="container">
	<div class="sixteen columns title-bar">
		<h1>Contact</h1>
		<span>Get In Touch</span>
	</div>

	<div class="one-third column">
		<?php print render($content); ?>
	</div>
	<div class="two-thirds column">
		<h3>Other ways to get in touch</h3>
		<ul class="large-list">
			<a href="mailto:contact@thisgoodworld.com" target="_blank">
				<li class="first">
					<i class="icon-envelope-alt"></i>Email</li>
			</a>
			<a href="http://www.facebook.com/thisgoodworld" target="_blank">
				<li>
					<i class="icon-facebook"></i>Facebook</li>
			</a>
			<a href="http://www.twitter.com/thisgoodworld" target="_blank">
				<li>
					<i class="icon-twitter"></i>Twitter</li>
			</a>
			<a href="http://www.instagram.com/thisgoodworld" target="_blank">
				<li>
					<i class="icon-camera"></i>Instagram</li>
			</a>
			<a href="https://foursquare.com/thisgoodworld" target="_blank">
				<li class="last">
					<i class="icon-globe"></i>Foursquare</li>
			</a>
		</ul>
	</div>
</div>