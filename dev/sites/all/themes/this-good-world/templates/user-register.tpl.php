
	<?php drupal_add_css(path_to_theme() . '/css/register.css', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>

  <?php drupal_add_js(path_to_theme() . '/js/register.js', array('group' => CSS_THEME, 'every_page' => FALSE)); ?>

		<?php
      print render($form['form_id']);
      print render($form['form_build_id']);
    ?>

		<div class="container">
      <div class="sixteen columns clearfix">
			<?php
				require_once(path_to_theme() . '/php/stripe-config.php');
				Stripe::setApiKey("sk_live_hPJhyRQiVLvfv4CEymo3MNYV");

				// Check for a form submission:
				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				// Stores errors:
				$errors = array();

				// Need a payment token:
				if (isset($_POST['stripeToken'])) {

					$token = $_POST['stripeToken'];

					// Check for a duplicate submission, just in case:
					// Uses sessions, you could use a cookie instead.
					if (isset($_SESSION['token']) && ($_SESSION['token'] == $token)) {
						$errors['token'] = 'You have apparently resubmitted the form. Please do not do that.';
					} else { // New submission.
						$_SESSION['token'] = $token;
					}

				} else {
					$errors['token'] = 'The order cannot be processed. Please make sure you have JavaScript enabled and try again.';
				}

				// Set the order amount somehow:
					if(isset($_POST['stripeAmount'])){
						$amount = $_POST['stripeAmount'];
					}


					if(isset($_POST['stripeEmail'])){
						$email = $_POST['stripeEmail'];
					}

					if(isset($_POST['planId'])){
						$plan_id = $_POST['planId'];
					}



				// Validate other form data!

				// If no errors, process the order:
				if (empty($errors)) {

					// create the charge on Stripe's servers - this will charge the user's card
					try {

						// Include the Stripe library:
						//require_once('includes/stripe/lib/Stripe.php');

						// set your secret key: remember to change this to your live secret key in production
						// see your keys here https://manage.stripe.com/account
						Stripe::setApiKey("sk_live_hPJhyRQiVLvfv4CEymo3MNYV");

						$customer = Stripe_Customer::create(array(
							"card" => $token,
							"plan" => $plan_id,
							"email" => $email)
						);

						// Charge the order:
						/*
						$charge = Stripe_Charge::create(array(
							"amount" => $amount, // amount in cents, again
							"currency" => "usd",
							"card" => $token,
							"description" => $email
							)
						);
						*/

						// Check that it was paid:

						if (!empty($customer->default_card)) {

							echo "<h3>Thanks! We got your payment. Go ahead and get started on your profile below.</h3>";
							// Store the order in the database.
							// Send the email.
							// Celebrate!

						} else { // Charge was not paid!
							echo '<div class="alert alert-error"><h4>Payment System Error!</h4>Your payment could NOT be processed (i.e., you have not been charged) because the payment system rejected the transaction. You can try again or use another card.</div>';
						}

					} catch (Stripe_CardError $e) {
							// Card was declined.
						$e_json = $e->getJsonBody();
						$err = $e_json['error'];
						$errors['stripe'] = $err['message'];
					} catch (Stripe_ApiConnectionError $e) {
							// Network problem, perhaps try again.
					} catch (Stripe_InvalidRequestError $e) {
							// You screwed up in your programming. Shouldn't happen!
					} catch (Stripe_ApiError $e) {
							// Stripe's servers are down!
					} catch (Stripe_CardError $e) {
							// Something else that's not the customer's fault.
					}

				} // A user form submission error occurred, handled below.

				} // Form submission.

			?>
      </div>

      <div class="sixteen columns title-bar">
        <h1>Registration</h1>
        <p class="instruction"><h2>This Good World Common Sense Clause:</h2></p>
<p class="instruction">
<em>
Rather than developing a checklist of requirements and dictating what you must do in order to be considered a “good” business, we leave it up to you to decide for yourselves. We do ask, however, that a common sense approach be taken when developing this profile. We reserve the right to ask for revisions or disapprove profile content upon submission.
</p>
<p class="instruction">
Please Do Not: Use your profile to post your latest promotion or deal, lie, share offensive information, etc.
</p>
<p class="instruction">
Please Do: Have fun, be creative and show us the good.</em>
</p>
<p class="instruction"><h2>Profile Information:</h2></p>
      </div>

      <div class="eight columns regular item">
				<div class="regular-display"><?php print render($form['group_account']['account']['name']); ?></div>
      </div>

      <div class="eight columns item">
				<div class="regular-display"><?php print render($form['group_account']['account']['mail']); ?></div>
      </div>

      <div class="eight columns regular item">
        <label class="show"><?php print render($form['group_account']['field_description']['und']['#title']); ?>  <span class="form-required" title="This field is required.">*</span></label>
        <p class="instruction"><?php print render($form['group_account']['field_description']['und']['#description']); ?> (<span id="counter">400</span> characters left)</p>
        <?php print render($form['group_account']['field_description']); ?>
      </div>

      <div class="eight columns item" id="logo">
        <label class="show"><?php print render($form['group_account']['field_logo']['und'][0]['#title']); ?> <span class="form-required" title="This field is required.">*</span></label>
        <p>Upload your logo. (minimum 250x250 pixels)</p>
        <?php print render($form['group_account']['field_logo']); ?>
      </div>

     <div class="sixteen columns clearfix">
        <label class="show"> <?php  print render($form['group_account']['field_type_of_business']['und']['select']['#title']); ?><span class="form-required" title="This field is required.">*</span></label>
         <p class="instruction"><?php print render($form['group_account']['field_type_of_business']['und']['#description']); ?> </p>
          <?php print render($form['group_account']['field_type_of_business']); ?>
      </div>

      <div class="sixteen columns clearfix">
				<h2>Your Good</h2>
      </div>

      <div class="sixteen columns item your-good">
        <label class="show"><?php print render($form['field_map_test']['und']['#title']); ?></label>
        <p class="instruction"><?php print render($form['field_map_test']['und']['#description']); ?></p>
        <?php print render($form['field_map_test']); ?>
      </div>

      <div class="eight columns item your-good">
        <label class="show"><?php print render($form['group_your_good']['field_for_the_environment']['und']['#title']); ?></label>
        <p class="instruction"><?php print render($form['group_your_good']['field_for_the_environment']['und']['#description']); ?></p>
        <?php print render($form['group_your_good']['field_for_the_environment']); ?>
       </div>

       <div class="eight columns item your-good">
        <label class="show"><?php print render($form['group_your_good']['field_for_social_change']['und']['#title']); ?></label>
        <p class="instruction"><?php print render($form['group_your_good']['field_for_social_change']['und']['#description']); ?></p>
        <?php print render($form['group_your_good']['field_for_social_change']); ?>
      </div>

      <div class="eight columns item your-good">
        <label class="show"><?php print render($form['group_your_good']['field_for_your_employees_culture']['und']['#title']); ?></label>
        <p class="instruction"><?php print render($form['group_your_good']['field_for_your_employees_culture']['und']['#description']); ?></p>
        <?php print render($form['group_your_good']['field_for_your_employees_culture']); ?>
      </div>

      <div class="eight columns item your-good">
        <label class="show"><?php print render($form['group_your_good']['field_for_your_community']['und']['#title']); ?></label>
        <p class="instruction"><?php print render($form['group_your_good']['field_for_your_community']['und']['#description']); ?></p>
        <?php print render($form['group_your_good']['field_for_your_community']); ?>
      </div>

      <div class="sixteen columns clearfix">
				<h2>Online</h2>
      </div>

      <div class="sixteen columns clearfix regular-display item">
				<?php print render($form['group_online']['field_website']); ?>
      </div>

      <div class="sixteen columns regular-display social">
	      <label class="show">Social Networks</label>
        <p class="instruction">Share your URLS so we can spread the word.</p>
      	<ul>
        	<li><label>Facebook</label></li>
          <li><label>Twitter</label></li>
          <li><label>Pinterest</label></li>
          <li><label>Foursquare</label></li>
          <li><label>Instagram</label></li>
          <li class="last"><label>LinkedIn</label></li>
        </ul>
      </div>

      <div class="sixteen columns clearfix regular-display social urls item">
				<?php print render($form['group_online']['group_social']['field_facebook_url']);?>
        <?php print render($form['group_online']['group_social']['field_twitter_url']); ?>
        <?php print render($form['group_online']['group_social']['field_pinterest_url']); ?>
        <?php print render($form['group_online']['group_social']['field_foursquare_url']);?>
        <?php print render($form['group_online']['group_social']['field_instagram_url']); ?>
        <?php print render($form['group_online']['group_social']['field_linkedin_url']);?>
      </div>

      <div class="sixteen columns clearfix">
				<h2>Address</h2>
      </div>
      <div class="eight columns clearfix item regular-display">
	      <?php print render($form['field_address']);?>
      </div>

      <div class="sixteen columns clearfix">
				<h2>Contacts</h2>
        <p class="instruction"><?php print render($form['field_contact_email']['und']['#description']); ?></p>
      </div>

      <div class="one-third column item">
        <label class="show"><?php print render($form['field_contact_email_2']['und']['#title']); ?></label>
        <?php print render($form['field_contact_email_2']); ?>
      </div>

      <div class="one-third column item">
        <label class="show"><?php print render($form['field_contact_email_3']['und']['#title']); ?></label>
        <?php print render($form['field_contact_email_3']); ?>
      </div>

      <div class="six column item">
        <label class="show"><?php print render($form['field_contact_email']['und']['#title']); ?></label>
        <?php print render($form['field_contact_email']); ?>
      </div>

      <div class="sixteen columns clearfix pledge">
      	<h2>Pledge  <span class="form-required" title="This field is required.">*</span></h2>
        <div class="item">
        	<?php print render($form['field_pledge']);	?>
          <p class="instruction"><?php print render($form['field_pledge']['und']['#description']);	?></p>
        </div>
      </div>

      <div class="one-third column clearfix">
      	<?php print drupal_render($form['actions']); ?>
      </div>

		</div> <!-- ./container -->

