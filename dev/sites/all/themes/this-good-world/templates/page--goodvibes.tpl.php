<?php drupal_add_css(path_to_theme() . '/css/blogs.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE)); ?>
	<div class="container">
		<div id="content" class="sixteen columns clearfix">
			<div id="main">
				<h1>Good Vibes</h1>
				<?php print render($page[ 'content']); ?>
				<div class="bumper"></div>
			</div>
		</div>
		<!-- /#content -->
		<div class="clear"></div>
	</div>
</div>