<?php //drupal_add_css(path_to_theme() . '/css/prettyPhoto.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE)); ?>
<?php drupal_add_css(path_to_theme() . '/rs-plugin/css/settings.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE)); ?>
<?php drupal_add_css(path_to_theme() . '/css/scoop.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE)); ?>
<?php drupal_add_css(path_to_theme() . '/css/mapbox.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE)); ?>
<?php drupal_add_css(path_to_theme() . '/css/magnific-popup.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE)); ?>
<?php drupal_add_css(path_to_theme() . '/css/home.css', array( 'group'=>CSS_THEME, 'every_page' => FALSE)); ?>

<?php if ($messages): ?>

<div id="messages">

	<?php print $messages; ?>

</div>
<!-- /#messages -->

<?php endif; ?>

<section class="home">
	<div class="super">
		<?php print $filters; ?>
		<ul id="supersized" class="quality" style="visibility: visible;">
			<li class="slide-0 activeslide">
				<?php echo views_embed_view( 'map', $display_id='page' ); ?>
                <!-- <a href="/getinvolved" id="get-involved-button"><img src="<?php //echo path_to_theme(); ?>/images/get-involved-button.png" width="200" height="200" alt="Get involved" /></a> -->
			</li>
		</ul>
	</div>
	<!-- ./super -->
</section>
<!-- ./home -->


</div>
<!-- ./sixteen columns -->

->

<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->


<!-- Mobile Specific Metas
  ================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


<!-- CSS
  ================================================== -->


<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

<!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/font-awesome-ie7.css">
		<style type="text/css">

			.page-wrapper,.footer{
				width: 100%;
				position: relative;
			}
			#break3{
				overflow: hidden;
			}

		</style>
	<![endif]-->

</head>

<body>
	<!-- Primary Page Layout
	================================================== -->



	<div class="page-wrapper" id="container">

<!-- 		<div class="container">
			<div class="sixteen columns">

				<div class="addthis_toolbox addthis_default_style addthis_32x32_style clearfix" style="margin-top: 1em;">
					<a class="addthis_button_twitter"></a>
					<a class="addthis_button_facebook"></a>
					<a class="addthis_button_email"></a>
				</div>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52e2e35f0b4dcba6"></script>

			</div>
		</div> -->

		<div id="profile-container"></div>

		<a name="the-goods" id="the-goods" class="menu-offset"></a>

		<section class="page-section blog-area">

	<!-- 		<div class="container">

				<div class="sixteen columns title-bar">

				</div>

			</div> -->
			<!-- ./container -->

			<div class="container">

				<div class="one-third column">
					<a href="/goodvibes" id="good-vibes-header" class="blog-section-header active">Good Things</a>
					<?php echo views_embed_view( 'good_blog_latest', $display_id='block' ); ?>
				</div>

				<div class="one-third column">
					<a href="/goodspotlight" id="good-spotlight-header" class="blog-section-header active">Good Spotlight</a>

					<?php echo views_embed_view( 'good_blog_latest', $display_id='block_3' ); ?>
				</div>

				<div class="one-third column">
					<a href="#" id="good-guides-header" class="blog-section-header active">member NEWS</a>
					<img src="<?php echo path_to_theme(); ?>/images/membernews1.jpg" alt="Member News- Coming soon" class="scale-with-grid">
					<?php //echo views_embed_view( 'good_blog_latest', $display_id='block_2' ); ?>
				</div>

			</div>
			<!-- ./container -->
		</section>
		<section class="page-section members-area">
			<!-- . members container -->
				<div class="container members-block">

				<div class="sixteen columns">

<a name="memberhomepage">

					<h3>MEMBERS</h3>
				</div>

				<div class="sixteen columns" id="logo-sets">

					<div class="four columns alpha">
						<div class="logo-container">
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/buffalo_TGW.png" alt="buffalo" class="logo-set scale-with-grid set-1" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/cuppow_TGW.png" alt="Cuppow" class="logo-set scale-with-grid set-2" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/allagash_TGW.png" alt="Allgash" class="logo-set scale-with-grid set-3" />
						</div>
					</div>
					<div class="four columns">
						<div class="logo-container">
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/empowermentplan_TGW.png" alt="empowerment plan" class="logo-set scale-with-grid set-1" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/detroitbikes_TGW.png" alt="Detroit bikes" class="logo-set scale-with-grid set-2" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/tattly_TGW.png" alt="Tattly - Designy Temporary Tattoos" class="logo-set scale-with-grid set-3" />
						</div>
					</div>
					<div class="four columns">
						<div class="logo-container">
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/makersrow_TGW.png" alt="Makersrow" class="logo-set scale-with-grid set-1" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/healthyliving_TGW.png" alt="Healty living" class="logo-set scale-with-grid set-2" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/sword_TGW.png" alt="Sword" class="logo-set scale-with-grid set-3" />

						</div>
					</div>
					<div class="four columns omega">
						<div class="logo-container">
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/stone_TGW.png" alt="stone" class="logo-set scale-with-grid set-1" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/steamhorse_TGW.png" alt="Steamhorse" class="logo-set scale-with-grid set-2" />
							<img src="<?php echo path_to_theme(); ?>/images/featured-members/v2/alloi_TGW.png" alt="Alloi" class="logo-set scale-with-grid set-3" />

						</div>
					</div>

				</div>
				<div class="sixteen columns">
					<h5>Find good in your neighborhood</h5>
				</div>
				<div class="eight offset-by-three columns">
					<ul class="cities clearfix">
						<li>Boston, MA</li>
						<li>Boulder, CO</li>
						<li>BURLINGTON, VT</li>
						<li>CHICAGO, IL</li>
						<li>DENVER, CO</li>
						<li>DETROIT, MI</li>
						<li>LOS ANGELES, CA</li>
					</ul>
				</div>
				<div class="five columns">
					<ul class="cities ralign clearfix">
						<li>NEW YORK, NY</li>
						<li>PHILADELPHIA, PA</li>
						<li>PORTLAND, ME</li>
						<li>PORTLAND, OR</li>
						<li>ROCHESTER, NY</li>
						<li>SAN DIEGO, CA</li>
						<li>SAN FRANCISCO, CA</li>
					</ul>
				</div>
				<div class="sixteen columns"> </a>
					<h6>Where to next?</h6>
					<a href="/contact" class="login-button">TELL US</a>
				</div>
			</div>
			<!-- .container -->
		</section>
		<!-- ./The Goods -->
		<section class="page-section mission">
			<div class="container">
				<div class="sixteen columns">
					<h5>Mission</h5>
					<div class="mission-text">Connect and support good businesses so you can discover and support them too</div>
					<a href="#" id="play-button">play</a>
				</div>
			</div>

		<?php if ($as_seen_on): ?>
			<section class="page-section asseenon">
			<div class="container">
					<div class="sixteen columns">
					<h5>AS SEEN ON</h5>
				</div>
				<div class="one-third columns">
		 				<img src="<?php echo path_to_theme(); ?>/images/seenonlogos/swissmiss_logo.png"  class="scale-with-grid" />
				</div>
					<div class="one-third column">
		 				<img src="<?php echo path_to_theme(); ?>/images/seenonlogos/npr_logo.png" class="scale-with-grid"  />
				</div>
					<div class="one-third column">
		 				<img src="<?php echo path_to_theme(); ?>/images/seenonlogos/ryot_logo.png" class="scale-with-grid" />

				</div>
			</div>
			</section>
			<?php endif; ?>
			<?php if ($social_footer): ?>
			<section class="page-section asseenon social-block">
			<div class="container">
					<div class="sixteen columns">
					<h5>FOLLOW</h5>
				</div>
				<div class="five columns">
		 				<a href="http://www.facebook.com/thisgoodworld">
		 				 <img src="<?php echo path_to_theme(); ?>/css/images/fb.png" class="scale-with-grid" />
			  		</a>
			  		<a href="http://www.twitter.com/thisgoodworld">
			  			<img src="<?php echo path_to_theme(); ?>/css/images/tw.png"   class="scale-with-grid"  />
			  		</a>
			  		<a href="http://www.instagram.com/thisgoodworld">
			  			<img src="<?php echo path_to_theme(); ?>/css/images/instag.png" class="scale-with-grid"  />
			  		</a>

				</div>
				</div>
			</section>
			<?php endif; ?>

		</section>


		<!-- ./about -->

	</div>
	<!-- ./page-wrapper -->

    <!-- PARALLAX -->
<!--     <section class="para" id="break3">
        <div class="overlay">
            <div class="container message">
                <p>Discover and Support Good Businesses</p>
            </div>
        </div>
    </section> -->
    <!-- END PARALLAX -->

<!-- start popup -->
    <div id="CoverPop-cover" class="splash">
        <div class="CoverPop-content splash-center">
        	<p class="large-text">DISCOVER BUSINESSES DOING GOOD THINGS.</p>
       		<div class="search-now CoverPop-close frstvisit">SEARCH NOW</div>
       		<div class="join-now">Are you a business doing good things? <a href="join"> JOIN HERE</a> </div>
        </div><!--end .splash-center -->
    </div><!--end .splash -->
    <!-- end popup -->


	<!-- End Document

================================================== -->









	<!-- JS

================================================== -->



	<?php
		drupal_add_js(path_to_theme() . '/js/jquery.scrollTo-1.4.2-min.js', array( 'group'=>CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/jquery.localscroll-1.2.7-min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/rs-plugin/js/jquery.themepunch.plugins.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/rs-plugin/js/jquery.themepunch.revolution.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/jquery.form.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/jquery.validate.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/scripts.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		//drupal_add_js(path_to_theme() . '/js/jquery.prettyPhoto.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/sizzle-main.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/jquery.infieldlabel.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/CoverPop.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/leaflet-pip.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/modernizr.custom.23663.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/jquery.magnific-popup.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/home.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/jquery-ui-1.8.7.custom.min.js', array('group' => CSS_THEME, 'every_page' => FALSE));
		drupal_add_js(path_to_theme() . '/js/mapbox.js', array('group' => CSS_THEME, 'every_page' => FALSE));
	?>
	<div class="clear"></div>

	</div>




	</div>
	<!-- /#wrap -->
