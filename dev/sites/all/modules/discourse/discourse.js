/**
 * @file
 * Monkey patch the Discourse Ember app.
 *
 * Provide client side modifications to allow the Discourse Ember application
 * to work via the server side Drupal discourse module as an intermediary to the main
 * Discourse forum server.
 */


// Namespace our functions.
Drupal.discourse = {};


// Tell Discourse to stop initialising until we are ready.
Discourse.deferReadiness();


/**
 * Add support for rendering specific Discourse Views in Drupal blocks.
 *
 * Move the Ember app to the document root so that it can receive events from
 * views anywhere in the DOM.
 * Then move the main application view back to where it is supposed to be.
 * Then move particular elements to placeholders in Drupal blocks provided by
 * this module.
 *
 * Experimental.
 */
Drupal.discourse.renderAsBlocks = function() {
    // Move the Ember app to the document root
    Discourse.rootElement = 'body';
    // Move Discourse main application view and subviews when rendered.
    Discourse.ApplicationView.reopen({
        didInsertElement: function() {
            $('body > .ember-view').appendTo($('section#main'));
            $('header.d-header').appendTo('#discourse-header-block');
        }
    });
    // Replicate Discourse's click handling for elements outside of div#main @see Discourse.bindDOMEvents
    $('body').not('section#main').on('click.discourse', '.ember-view a, [href="/discourse"]', function(e) {
        if (e.isDefaultPrevented() || e.shiftKey || e.metaKey || e.ctrlKey) return;

        var $currentTarget = $(e.currentTarget);
        var href = $currentTarget.attr('href');
        if (!href) return;
        if (href === '#') return;
        if ($currentTarget.attr('target')) return;
        if ($currentTarget.data('auto-route')) return;

        // If it's an ember #linkTo skip it
        if ($currentTarget.hasClass('ember-view')) return;

        if ($currentTarget.hasClass('lightbox')) return;
        if (href.indexOf("mailto:") === 0) return;
        if (href.match(/^http[s]?:\/\//i) && !href.match(new RegExp("^http:\\/\\/" + window.location.hostname, "i"))) return;

        e.preventDefault();
        Discourse.URL.routeTo(href);
        return false;
    });
};


/*
 * Rewrite html string before render.
 * Adds a prefix to link href.
 * Adds original domain name to image src.
 * @todo improve the way we check if this string requires rewriting of links.
 *
 * @param html String
 * @return String
 */
Drupal.discourse.rewriteHtml = function(html) {
    // If this is plain text then we don't need to rewrite anything.
    if (html.trim().slice(0,1) !== '<') {return html}
    // If jQuery can't parse the string then return the string unaltered.
    var $html;
    try {
        $html = $(html);
    } catch(e){
        return html
    }
    var server = Drupal.settings.discourse.server;
    $('a', $html).each(function(index, el) {
        var href = $(el).attr('href');
        // Some Discourse links will already be correct as they respect the rootUrl, others are hardcoded.
        if (href && href.slice(0,1) === '/' && href.slice(0, Drupal.settings.discourse.rootURL.length) !== Drupal.settings.discourse.rootURL) {
            $(el).attr('href', Drupal.settings.discourse.rootURL + href);
        }
    });
    $('img', $html).each(function(index, el) {
        var src = $(el).attr('src');
        if (src && src.slice(0,1) === '/') {
            $(el).attr('src', server + src);
        }
    });
    // Jquery has no native outerHTML function.
    var str = '';
    $.each($html, function(index, value) {
        if (value.outerHTML) {
            str += value.outerHTML;
        }
    });
    return str;
};


/**
 * Rewrite the full path to remove the Drupal subpath and return the Ember path.
 * @param path String
 * @return String
 */
Drupal.discourse.rewritePath = function(path) {
    path = path.replace(/https?\:\/\/[^\/]+/, '');
    // TODO fix this regex so it only replaces at the start of the path.
    var rootURLregex = Drupal.settings.discourse.rootURL.replace(/\/$/, '');
    return path.replace(rootURLregex, '');
};


$(document).ready(function() {
    // Alter any Discourse ajax calls that they have the csrf token added to the header.
    var csrfToken = $('meta[name=csrf-token]').attr('content');
    var isDrupalAjaxCall = function(options) {
        // See if the beforeSend callback contains Drupal code. @see Drupal.ajax
        return (options.beforeSend && options.beforeSend.toString().indexOf('ajax.ajaxing') !== -1);
    };
    $.ajaxPrefilter(function(options, originalOptions, xhr) {
        if (!isDrupalAjaxCall(options)) {
            // @todo remove this once Discourse supports subdirectories fully.
            if (options.url.indexOf(Drupal.settings.discourse.rootURL) == -1) {
                options.url = Drupal.settings.discourse.rootURL + options.url;
            }
            // Put the csrf token in. This is cross domain so Discourse own code will not add it.
            xhr.setRequestHeader('X-CSRF-Token', csrfToken);
        }
    });
    if (Drupal.settings.discourse.blocks) {
        Drupal.discourse.renderAsBlocks();
    }
    // Allow Discourse to continue initialising now that we've made our modifications.
    Discourse.advanceReadiness();
});


/*
 * Patch Discourse.ready so we can change the rootURL for the Discourse app.
 */
Drupal.discourse._ready = Discourse.ready;
Discourse.ready = function() {

    // Set the root URL
    Discourse.BaseUri = Drupal.settings.discourse.rootURL;

    // Patch Ember so that links are rewritten to include prefix and base path during the render cycle.
    Ember._RenderBuffer.prototype.string = function() {
        if (this._element) {
            return Drupal.discourse.rewriteHtml(this.element().outerHTML);
        } else {
            return Drupal.discourse.rewriteHtml(this.innerString());
        }
    };

    // Patch this render method so that we can rewrite the HeaderView.logoHtml function.
    Ember._SimpleHandlebarsView.prototype.render = function() {
        // If not invoked via a triple-mustache ({{{foo}}}), escape
        // the content of the template.
        var escape = this.isEscaped;
        var result = this.normalizedValue();

        if (result === null || result === undefined) {
            result = "";
        } else if (!(result instanceof Handlebars.SafeString)) {
            result = String(result);
        }

        if (escape) { result = Handlebars.Utils.escapeExpression(result); }
        return Drupal.discourse.rewriteHtml(result);
    };

    // Patch Discourse to remove Drupal base path and forum prefix.
    Discourse.URL._routeTo = Discourse.URL.routeTo;
    Discourse.URL.routeTo = function(path) {
        path = Drupal.discourse.rewritePath(path);
        return Discourse.URL._routeTo(path);
    };

    // Patch Discourse to remove Drupal base path and forum prefix.
    Discourse.URL._replaceState = Discourse.URL.replaceState;
    Discourse.URL.replaceState = function (path) {
        path = Drupal.discourse.rewritePath(path);
        Discourse.URL._replaceState(path);
    };

    Drupal.discourse.discourse_location = Ember.get(Discourse.__container__.lookup('router:main'), 'location');
    Drupal.discourse.discourse_location.rootURL = Drupal.settings.discourse.rootURL;
    Drupal.discourse.discourse_location.onUpdateURL = function(callback) {
        var guid = Ember.guidFor(this),
            self = this;
        Ember.$(window).bind('popstate.ember-location-' + guid, function(e) {
            if (e.state) {
                var currentState = self.get('currentState');
                if (currentState) {
                    e.state.path = Drupal.discourse.rewritePath(e.state.path);
                    callback(e.state.path);
                } else {
                    this.set('currentState', e.state);
                }
            }

        });
    };
    Drupal.discourse._ready();
};



receiveMessage = function(event) {
    if (event.origin !== Drupal.settings.discourse.server) {return}
    Discourse.authenticationComplete(event.data);
};

window.addEventListener("message", receiveMessage, false);

