<?php

/**
 * @file Handles external auth providers
 */


/**
 * Create a Discourse account and log in the user to the new account.
 *
 * @param array $params
 *
 * @todo Manage the situation where admin approval is required.
 * @todo Handle error case when submitted email does not match verified email.
 */
function discourse_auth_create_user($params) {
  $json_data = array();
  // Case where external auth has provided a confirmed email.
  if (isset($_SESSION['discourse']['auth']['data']['email'])
    && $_POST['email'] == $_SESSION['discourse']['auth']['data']['email']) {
    include_once(drupal_get_path('module', 'discourse') . '/includes/discourse_user.inc');
    $user_id = discourse_create_user($params);
    $json_data = array(
        'success' => TRUE,
        'active' => TRUE,
        'message' => t('Your account is active and ready'),
    );
    $discourse_user = discourse_get_discourse_user_by_params(array('id' => $user_id));
    if ($discourse_user) {
      discourse_login($discourse_user);
      // We're done with session saved auth data now.
      unset($_SESSION['discourse']['auth']);
    }
  }
  // Twitter external auth
  if (isset($_SESSION['discourse']['auth']['twitter'])) {
    include('twitter.inc');
    $json_data = discourse_auth_twitter_create_user($params);
  }
  // Return the ajax response.
  drupal_json_output($json_data);
  exit;
}


/**
 * External auth complete.
 *
 * @see /discourse/app/views/users/omniauth_callbacks/complete.html.erb
 */
function discourse_auth_complete($data) {
  $data_json = json_encode($data);
  print <<< EOF
    <html>
    <head></head>
    <body>
      <script type="text/javascript">
        window.opener.Discourse.authenticationComplete({$data_json});
        window.close();
      </script>
    </body>
    </html>
EOF;
  drupal_exit();
}


/**
 * External auth failure.
 *
 * This will need to be fleshed out once Discourse itself handles these.
 */
function discourse_auth_failure() {
  print "Authentication failed";
  drupal_exit();
}