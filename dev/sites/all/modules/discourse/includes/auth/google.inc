<?php

/**
 * @file Handle Google OpenID external auth.
 */

include('common.inc');

/**
 * Setup Google OpenID redirect.
 */
function discourse_auth_google() {
  global $base_secure_url, $base_insecure_url;
  include(drupal_get_path('module', 'discourse') . '/lib/lightopenid.inc');
  $host = str_replace('http://', '', $base_insecure_url);
  $openid = new LightOpenID($host);
  $openid->realm     = (!empty($_SERVER['HTTPS'])) ? $base_secure_url : $base_insecure_url;
  $openid->returnUrl = $openid->realm . base_path() . variable_get('discourse_forum_root') .'/auth/gcb';
  $openid->required = array(
    'namePerson',
    'namePerson/first',
    'namePerson/last',
    'contact/email',
  );
  $openid->optional = array(
    'namePerson/first',
    'contact/city/home',
    'contact/state/home',
    'contact/web/default',
    'media/image/aspect11',
    'postcode',
    'nickname',
  );
  $openid->identity = 'https://www.google.com/accounts/o8/id';
  $location =  $openid->authUrl();
  header('Location: ' . $location);
  exit;
}


/**
 * Handle Google OpenID callback.
 */
function discourse_auth_google_callback() {
  include_once(drupal_get_path('module', 'discourse') .'/includes/discourse_user.inc');
  include(drupal_get_path('module', 'discourse') . '/lib/lightopenid.inc');
  global $base_insecure_url;
  $host = str_replace('http://', '', $base_insecure_url);
  $openid = new LightOpenID($host);
  if ( $openid->mode && $openid->validate() ) {
    $attributes = $openid->getAttributes();
    $discourse_user = discourse_get_discourse_user_by_params(array('email' => $attributes['contact/email']));
    if ($discourse_user) {
      discourse_login($discourse_user);
      $data = array('authenticated' => TRUE);
    }
    else {
      $data = array(
        'email' => $attributes['contact/email'],
        'name' => $attributes['namePerson/first'] . ' ' . $attributes['namePerson/last'],
        'email_valid' => TRUE,
        'auth_provider' => 'Google',
      );
      $data['username'] = discourse_get_discourse_username($data['name']);
      // Store the authed data so we can check later on account creation request.
      $_SESSION['discourse']['auth']['data'] = $data;
    }
    discourse_auth_complete($data);
  }
}

