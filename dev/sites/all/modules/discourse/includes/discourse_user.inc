<?php

/**
 * @file
 *
 * Provide sync between Drupal and Discourse users.
 *
 * This will require Drupal to have access to the Discourse Postgres database.
 */

// @todo check when Drupal email is updated and update Discourse email
// @todo Log out Drupal user when Discourse user logs out.

/**
 * Log in the current Discourse user as the corresponding Drupal user.
 *
 */
function discourse_login_drupal_user() {
  global $user;
  if (!$user->uid) {
    return;
  }
  $discourse_user = discourse_get_discourse_user($user);
  if ($discourse_user) {
    discourse_login($discourse_user);
  }
}


function discourse_login($discourse_user) {
  if ($discourse_user) {
    // We need to set the token cookie and clear the discourse session cookie to avoid a conflict in CurrentUser->current_user
    setcookie('_t', $discourse_user['auth_token'], NULL, '/', NULL, NULL, TRUE);
    setcookie('_forum_session', '', 0, '/', NULL, NULL, TRUE);
  }
}


/**
 * Log out the current user from Discourse.
 */
function discourse_logout() {
  setcookie('_t', '', 0, '/', NULL, NULL, TRUE);
  setcookie('_forum_session', '', 0, '/', NULL, NULL, TRUE);
}


/**
 * Ensure Discourse accounts exist for all existing Drupal users.
 */
function discourse_drupal_user_sync_now() {
  $accounts = user_load_multiple(FALSE);
  $n = 0;
  foreach ($accounts as $account) {
    if (!discourse_get_discourse_uid_by_mail($account)) {
      if ($account->uid && $account->status) {
        $username = discourse_get_discourse_username($account->name);
        $params = array(
          'name' => str_pad($account->name, 3, "_", STR_PAD_BOTH),
          'username' => $username,
          'email' => $account->mail,
          'password' => user_password(),
        );
        $discourse_account = discourse_create_user($params, $account);
        if ($discourse_account) {
          $n++;
        }
      }
    }
  }
  drupal_set_message(t(':n new Discourse accounts were created', array(':n' => $n)), 'status');
}


/**
 * Retrieve the matching Discourse user id for a given Drupal account.
 *
 * @param $account
 *   Drupal user object
 * @return int
 *   Discourse user id if a match exists, 0 otherwise.
 */
function discourse_get_discourse_uid($account) {
  if (!$account->uid) {
    return 0;
  }
  // First check if we have an association.
  $discourse_uid = db_select('discourse_users', 'du')
    ->fields('du', array('discourse_uid'))
    ->condition('uid', array($account->uid))
    ->execute()
    ->fetchField();
  if ($discourse_uid) {
    return $discourse_uid;
  }

  // Next check if we have a matching email.
  return discourse_get_discourse_uid_by_mail($account);
}

/**
 * Retrieve the matching Discourse user id for a given Drupal account by matching email.
 *
 * @param $account
 *   Drupal user object
 * @return int
 *   Discourse user id if a match exists, 0 otherwise.
 */
function discourse_get_discourse_uid_by_mail($account) {
  if (!$account->uid) {
    return 0;
  }
  // Check if we have a matching email.
  db_set_active('discourse');
  $discourse_uid = db_select('users', 'u')
    ->fields('u', array('id'))
    ->condition('email', $account->mail)
    ->execute()
    ->fetchField();
  db_set_active('default');
  if ($discourse_uid) {
    // We should add an entry into our sync table.
    discourse_register_sync($account, $discourse_uid);
    return $discourse_uid;
  }
  return 0;
}

/**
 * Register an association between a Drupal user and a Discourse user.
 *
 * @param object $account
 *     Drupal user object
 * @param int $discourse_uid
 *     Discourse user id
 */
function discourse_register_sync($account, $discourse_uid) {
  db_merge('discourse_users')
    ->key(array('uid' => $account->uid))
    ->fields(array(
      'discourse_uid' => $discourse_uid,
    ))
    ->execute();
}



/**
 * Retrieve the matching Discourse user for a given Drupal account.
 *
 * @param $account
 *   Drupal user object
 * @return mixed
 *   Discourse user as an array if found, FALSE otherwise.
 */
function discourse_get_discourse_user($account) {
  $discourse_uid = discourse_get_discourse_uid($account);
  if ($discourse_uid) {
    return discourse_get_discourse_user_by_params(array('id' => $discourse_uid));
  }
  else {
    return FALSE;
  }
}


/**
 * Retrieve the matching Discourse user for a given Drupal account.
 *
 * Matches by username.
 *
 * @param $params
 *   Array of params to match on keyed by column name.
 * @return array|bool
 *   Discourse user as an array if found, FALSE otherwise.
 */
function discourse_get_discourse_user_by_params($params) {
  db_set_active('discourse');
  $query = db_select('users', 'u')
    ->fields('u');
  foreach ($params as $key => $value) {
    $query->condition('u.' . $key, $value);
  }
  $discourse_user = $query->execute()
    ->fetchAssoc();
  db_set_active('default');
  return $discourse_user;
}


/**
 * Generate a valid and available Discourse username given a Drupal username.
 *
 * @param $name
 * @return string
 */
function discourse_get_discourse_username($name) {
  $discourse_username = preg_replace('/[^0-9,^a-z]/', '', strtolower($name));
  return discourse_check_discourse_username($discourse_username);
}


/**
 * Check a Discourse username is available and supply an alternative if not.
 *
 * @param string $name
 *     Proposed discourse username
 * @return string
 *     Available discourse username
 */
function discourse_check_discourse_username($name) {
  $discourse_server = variable_get('discourse_server');
  $options = array(
    'headers' => array(
      'X-Requested-With' => 'XMLHttpRequest',
      'Accept' => '*/*',
    )
  );
  $url = $discourse_server . '/users/check_username?username=' . $name;
  $response = discourse_http_request($url, $options);
  $data = json_decode($response->data);
  if ($data->available) {
    return $name;
  }
  else {
    return $data->suggestion;
  }
}


/**
 * Create a Discourse user by adding a row to the Discourse database.
 *
 * @param array $params
 *     POST array.
 * @param object $account
 *     Drupal user account object.
 * @param $options
 *     Options for the user account, should not come from user input.
 *
 * @return int
 *     Discourse user id.
 */
function discourse_create_user($params, $account = NULL, $options = array()) {
  // Allow other modules to modify the params before creating a user.
  foreach (module_implements('discourse_create_account') as $module) {
    $function = $module . '_discourse_create_account';
    $function($params, $options);
  }
  if (function_exists('mcrypt_create_iv')) {
    $auth_token = mcrypt_create_iv(32, MCRYPT_DEV_RANDOM);
  }
  else {
    drupal_set_message('Could not create a Discourse user. Please review the module requirements in the README.');
  }
  db_set_active('discourse');
  $query = db_insert('users');
  $time = date('Y-m-d H:i:s.u');
  $fields = array(
    'username' => $params['username'],
    'name' => $params['name'],
    'username_lower' => strtolower($params['username']),
    'email' => $params['email'],
    'active' => isset($options['active'])? $options['active'] : TRUE,
    'trust_level' => isset($options['trust_level'])? $options['trust_level'] : 1,
    'created_at' => $time,
    'updated_at' => $time,
    'auth_token' => $auth_token,
  );
  $query->fields($fields);
  $discourse_uid = $query->execute();
  db_set_active('default');
  if ($account && $discourse_uid) {
    discourse_register_sync($account, $discourse_uid);
  }
  return $discourse_uid;
}
