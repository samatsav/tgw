# Drupal - Discourse integration

This module provides a way to embed a Discourse forum into a Drupal website.

## Installation

* Download and enable this module, and the querypath module.
* Set the discourse server to use at /admin/config/system/discourse
* Log into the Discourse instance as an admin and go to the site settings screen /admin/site_settings
  Set force_hostname to be the Drupal url and path in Discourse site settings.
  Eg drupalsite.com/discourse This will give correct links in emails sent by Discourse.
* Your forum should now be available at /discourse

## Discourse version

Currently tested with
* Discourse Version: 0.9.4

To update Discourse to this version
    git fetch
    git checkout v0.9.4
    rake db:migrate


## User syncing

For user syncing between Drupal and Discourse, Drupal will need to have access
to the Discourse PGSQL database. Add the details as follows to your settings.php

$databases['discourse']['default'] = array(
  'driver' => 'pgsql',
  'database' => 'databasename',
  'username' => 'username',
  'password' => 'password',
  'host' => 'localhost',
  'prefix' => '',
);

If PostgresSQL is on another server you will have to allow access from the
ip address your Drupal server is running on.

You should edit the listen_address property in the postgresql.conf file.

This directory path for the postgresql.conf file will be something like

/etc/postgresql/9.1/main/

User integration requires PHP tp be compiled with mcrypt_create_iv function available.
http://php.net/manual/en/function.mcrypt-create-iv.php

## Known limitations

* You cannot use a root forum path that matches a root path in Discourse
  e.g. /t /category etc.
* The Discourse pages load themselves with javascript rather than page reloads,
  so any Drupal content (blocks etc.) will need to be consistent across the
  Discourse forum.
* If using 'drush rs' to run the server, you will need to ensure you use the cgi
  version. 'drush rs server=cgi' Otherwise paths like
  'http://127.0.0.1:8888/discourse/t/25/1.json' do not reach the Drupal stack
  but instead receive a 404 response from the PHP Built-in server.
* If using 'drush rs' to run the server user sync between Drupal and Discourse will
  not work. Drush RS has a bug where any call to setcookie wipes out any previously
  set cookies. This prevents Drupal logins amongst other bugs.
* Discourse screens will have jQuery set to version 1.8. This should not cause any issues
  but you may see errors if your javascript code uses deprecated jQuery functions.

## Roadmap

* Performance optimisations, avoid bootstrapping entire Drupal stack on each proxy call.
