<?php

/**
 * @file Documents available hooks.
 */


/**
 * Implements hook_discourse_create_account().
 *
 * Alter the params array to change any of the described values
 * before the Drupal users are synced to Discourse. The typical use
 * case would be to use a real name field to replace $params['name']
 */
function hook_discourse_create_account(&$params, &$options) {
  $params['name'] = 'new value'; //str
  $params['username'] = 'new value'; //str
  $params['email'] = 'new value'; //str
  $options['trust_level'] = 'new value'; //int
  $options['active'] = TRUE; //bool
}
